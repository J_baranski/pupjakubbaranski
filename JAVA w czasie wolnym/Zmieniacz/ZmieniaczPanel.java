import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
*Klasa stanowi content okna programu
*/

public class ZmieniaczPanel extends JPanel implements ActionListener {

	private JTextField fileInput;
	private JTextArea textArea;
	String outputFileName = "";
	boolean isLoaded = false;
	/**
	 * Defining buttons, so they can be later changed to disabled and enabled
	 */
	JButton change;
	JButton reset;
	JButton fileChooser;
	JLabel statusLabel;

	/**
	 * Constructor creates a panel and sets layout to it. Nothing big.
	 */
	public ZmieniaczPanel() {
		setBackground(Color.GRAY);
		setPreferredSize(new Dimension(400, 400));
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		fileInput = new JTextField("Wybierz plik...", 33);
		fileInput.setBackground(Color.WHITE);
		fileInput.setBorder(BorderFactory.createLoweredBevelBorder());
		fileInput.addActionListener(this);
		fileInput.setActionCommand("enter");
		fileInput.setEditable(false);

		fileChooser = new JButton("Przeglądaj");
		fileChooser.setBackground(Color.LIGHT_GRAY);
		fileChooser.addActionListener(this);

		textArea = new JTextArea(10, 10);
		textArea.setBackground(Color.LIGHT_GRAY);
		textArea.setBorder(BorderFactory.createLoweredBevelBorder());
		textArea.setEditable(false);

		JScrollPane scroller = new JScrollPane(textArea);

		JPanel label = new JPanel();
		label.setLayout(new FlowLayout(FlowLayout.LEFT));
		label.add(new JLabel("Wpisz nazwę pliku"));
		label.add(fileChooser);
		JPanel input = new JPanel();
		input.add(fileInput);
		input.setLayout(new FlowLayout(FlowLayout.LEFT));

		JPanel labelAndInput = new JPanel();
		labelAndInput.setLayout(new GridLayout(2, 0));
		labelAndInput.add(label);
		labelAndInput.add(input);

		JPanel textAreaAndButtons = new JPanel();
		textAreaAndButtons.setLayout(new BorderLayout(5, 5));
		textAreaAndButtons.add(scroller, BorderLayout.CENTER);
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(5, 0, 20, 20));
		change = new JButton("Zmień i zapisz");
		change.setBackground(Color.LIGHT_GRAY);
		change.addActionListener(this);
		change.setEnabled(false);
		reset = new JButton("Wyczyść");
		reset.setBackground(Color.LIGHT_GRAY);
		reset.addActionListener(this);
		reset.setEnabled(false);
		buttonPanel.add(change);
		buttonPanel.add(reset);
		textAreaAndButtons.add(buttonPanel, BorderLayout.EAST);

		statusLabel = new JLabel("Ready");

		setLayout(new BorderLayout(3, 3));
		add(labelAndInput, BorderLayout.NORTH);
		add(textAreaAndButtons, BorderLayout.CENTER);
		add(statusLabel, BorderLayout.SOUTH);

		fileInput.requestFocus();
	}

	void updateButtons() {
		if (isLoaded == false) {
			change.setEnabled(false);
			reset.setEnabled(false);

		} else {
			change.setEnabled(true);
			reset.setEnabled(true);

		}
	}

	String changeLetters(String wczytanyTekst) {
		// wczytanyTekst = wczytanyTekst.replace("Ăł", "ó"); // ó jest chyba w
		// porządku...
		wczytanyTekst = wczytanyTekst.replace("ê", "ę");
		wczytanyTekst = wczytanyTekst.replace("¹", "ą");
		wczytanyTekst = wczytanyTekst.replace("ê", "ę");
		wczytanyTekst = wczytanyTekst.replace("³", "ł");
		wczytanyTekst = wczytanyTekst.replace("œ", "ś");
		wczytanyTekst = wczytanyTekst.replace("¿", "ż");
		wczytanyTekst = wczytanyTekst.replace("æ", "ć");
		wczytanyTekst = wczytanyTekst.replace("Ÿ", "ź");
		wczytanyTekst = wczytanyTekst.replace("ñ", "ń");
		wczytanyTekst = wczytanyTekst.replace("Ê", "Ę");
		wczytanyTekst = wczytanyTekst.replace("Œ", "Ś");
		wczytanyTekst = wczytanyTekst.replace("Æ", "Ć");
		wczytanyTekst = wczytanyTekst.replace("¥", "Ą");
		wczytanyTekst = wczytanyTekst.replace("Ñ", "Ń");
		wczytanyTekst = wczytanyTekst.replace("£", "Ł");
		wczytanyTekst = wczytanyTekst.replace("¯", "Ż");
		wczytanyTekst = wczytanyTekst.replace("  ", "Ź"); // TODO!!

		return wczytanyTekst;
	}

	public void actionPerformed(ActionEvent evt) {

		String op = evt.getActionCommand();

		File plik; // tworzy zmienna pliku wejsciowego

		if (op.equals("Przeglądaj")) {
			JFileChooser selectedFile = new JFileChooser();
			selectedFile.setApproveButtonText("Otwórz");
			selectedFile.setDialogTitle("Wybierz plik .txt zawierający napisy");
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Pliki tekstowe .txt", "txt", "text");
			selectedFile.setFileFilter(filter);

			int returnValue = selectedFile.showOpenDialog(null);
			if (returnValue == JFileChooser.APPROVE_OPTION) {

				String nazwa = "";
				try {
					plik = selectedFile.getSelectedFile(); // przypisauje
															// wartosc zmiennej
															// pliku zgodnie z
															// wybranym plikiem
					nazwa = selectedFile.getSelectedFile().getAbsolutePath(); // wczytuje
																				// nazwę
																				// pliku
					//nazwa = plik.getAbsolutePath(); 

					fileInput.setText(nazwa); // wpisuje nazwe pliku w pole
												// wyboru
					Scanner czytacz = new Scanner(plik, "UTF-8");
					while (czytacz.hasNextLine()) {
						textArea.append(czytacz.nextLine());
						textArea.append("\n");

					}
					isLoaded = true;
					updateButtons();
					czytacz.close();

				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(this, "Taki plik nie istnieje...");
					fileInput.setText("Wybierz plik...");
					statusLabel.setText("Ready");
				}
				if (isLoaded == true) {
					outputFileName = nazwa;
					statusLabel.setText("Wczytano plik: " + outputFileName);
				}
			}

		}

		else if (op.equals("Zmień i zapisz")) {

			String wczytanyTekst = textArea.getText();
			textArea.setText(changeLetters(wczytanyTekst));

			if (JOptionPane.showConfirmDialog(null, "Plik zostanie nadpisany.\nKontynuować?", "Potwierdzenie",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

				File plik2 = new File("/" + outputFileName);
				File plikOut = new File("/" + outputFileName + "mod");
				try {
					Scanner wczytywacz = new Scanner(plik2, "UTF-8");
					PrintWriter wpisywacz = new PrintWriter(plikOut, "UTF-8");

					while (wczytywacz.hasNextLine()) {
						wpisywacz.println(changeLetters(wczytywacz.nextLine()));
						wpisywacz.flush();
					}
					wpisywacz.close();
					wczytywacz.close();

					// plik.setWritable(true);
					plik2.delete();
					plikOut.renameTo(plik2);

					statusLabel.setText("Zapisane i gotowe!");

				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Błąd: Nie znaleziono pliku");
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					JOptionPane.showMessageDialog(null, "Błąd: Nieobsługiwane kodowanie");
					e.printStackTrace();
				}
			} else {
				textArea.setText(wczytanyTekst);
			}

		} else { // czyli przycisk wyczysc!
			fileInput.setText("Wybierz plik...");
			textArea.setText("");
			isLoaded = false;
			outputFileName = "";
			statusLabel.setText("Ready");
			updateButtons();
		}
		updateButtons();
	}
}
