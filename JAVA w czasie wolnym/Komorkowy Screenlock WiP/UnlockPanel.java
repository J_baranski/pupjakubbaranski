
/**
 * G��wna czesc programu znajduje sie tutaj. panel stanowi kontent JFrame'a stworzonego w glownej metodzie programu.
 * Klasa ta zawiera takze zagnie�d�on� klas� UnlockCanvas, ktora stanowi pole do rysowania wzoru. Klasa ta ma dwie zmienne sta�e ROWS i COLS, 
 * ktore mog� zostac zmienione w celu stworzenia bardziej skomplikowanej sekwencji wzoru.
 */

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.*;

public class UnlockPanel extends JPanel {

	JButton setNew;
	JButton confirm;
	JButton cancel;
	JLabel message;
	boolean settingMode;
	ArrayList<Point> activePoints;
	ArrayList<Point> code;
	boolean dragging;
	int x,y;

	UnlockPanel() {

		setLayout(new BorderLayout(0, 5));
		setPreferredSize(new Dimension(500, 500));
		JPanel buttonPanel = new JPanel();
		setNew = new JButton("Zmien wz�r");
		confirm = new JButton("Potwierd�");
		confirm.setEnabled(false);
		cancel = new JButton("Anuluj");
		cancel.setEnabled(false);
		buttonPanel.add(setNew);
		buttonPanel.add(confirm);
		buttonPanel.add(cancel);

		message = new JLabel("Wprowadz wzor, aby odblokowac", JLabel.CENTER);
		message.setFont(new Font("SansSerif", Font.PLAIN, 20));
		UnlockCanvas canvas = new UnlockCanvas();
		activePoints = new ArrayList<Point>(); // zawiera punkty. Kolejnosc
												// punktow bedzie decydowala
												// o poprawnosci wzoru
		setNew.addActionListener(canvas);
		cancel.addActionListener(canvas);
		confirm.addActionListener(canvas);
		add(message, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.SOUTH);
		add(canvas, BorderLayout.CENTER);

	}

	private class UnlockCanvas extends JPanel implements MouseListener, MouseMotionListener, ActionListener {
		/**
		 * Okreslaj� ilo�� punktow (+1), Przy zmianie tych stalych, nalezy
		 * pamietac, ze moze byc konieczne zmniejszenie czulosci wykrywanioa
		 * punktow w metodzie searchForPoint(), a takze promienia rysowanego
		 * okregu. Przesadzenie z tymi wartosciami najprawdopodobniej zje ca��
		 * pamiec, wiec spokojnie...
		 */
		final int ROWS = 4;
		final int COLS = 4;

		/**
		 * okresla odleglosc w pikselach od srodka punktu w jakiej muli znalesc
		 * sie wskaznik myszy, aby dany punkt zosta� wybrany i dodany do listy
		 * aktywnych elementow. Stanowi tez podstawe promienia rysowanego wokol
		 * punktu okregu
		 */
		final int PRECISION = 40; // okresla odleglosc w pikselach od srodka
									// punktu w jakiej muli znalesc sie wskaznik
									// myszy, aby dany punkt zosta� wybrany i
									// dodany do listy aktywnych elementow.
									// Stanowi tez podstawe promienia rysowanego
									// wokol punktu okregu

		/**
		 * Konstruktor nadaje kolor tla i dodaje MouseListenery
		 */
		private UnlockCanvas() {
			setBackground(Color.WHITE);
			addMouseListener(this);
			addMouseMotionListener(this);
			repaint();
			setBorder(BorderFactory.createLineBorder(Color.GRAY, 20));
			settingMode = false;
		}

		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setStroke(new BasicStroke(3)); // pogrubia linie
			// rysuje kropki
			g.setColor(Color.BLACK);
			for (int row = 0; row < ROWS - 1; row++) {
				for (int col = 0; col < COLS - 1; col++) {
					g.fillOval((getWidth() / COLS * (col + 1) - 4), (getHeight() / ROWS * (row + 1) - 4), 8, 8);
				}
			}
			// rysuje owal w okol aktywnego punktu i cyfre wskazujaca na kolejnosc.
			g.setColor(Color.GREEN);
			for (int i = 0; i < activePoints.size(); i++) {
				g.drawOval(getWidth() / COLS * (activePoints.get(i).y + 1) - PRECISION,
						getHeight() / ROWS * (activePoints.get(i).x + 1) - PRECISION, PRECISION * 2, PRECISION * 2);
				g.drawString(""+(i+1), getWidth() / COLS * (activePoints.get(i).y + 1) - PRECISION, getHeight() / ROWS * (activePoints.get(i).x + 1) - PRECISION);

			}
			// rysuje linie laczace wybrane punkty, jezeli takowe istnieja
			if (activePoints.size() > 0) {
				g.setColor(Color.BLACK);
				for (int i = 1; i < activePoints.size(); i++) {
					g.drawLine(getWidth() / COLS * (activePoints.get(i - 1).y + 1),
							getHeight() / ROWS * (activePoints.get(i - 1).x + 1),
							getWidth() / COLS * (activePoints.get(i).y + 1),
							getHeight() / ROWS * (activePoints.get(i).x + 1));
				}
			}

			// rysuje linie od ostatniego punktu do wskaznika myszy
			if(dragging){
				g.setColor(Color.BLACK);
				g.drawLine(getWidth() / COLS * (activePoints.get(activePoints.size() - 1).y + 1),
						getHeight() / ROWS * (activePoints.get(activePoints.size() - 1).x + 1), x, y);
			}
		}

		/**
		 * Metoda wywo�ywana przez MouseListener ktora okresla ktory punkt
		 * zostal wybrany. Kozysta ze sta�ej PRECISION okreslaj�cej dokladnosc z
		 * jak� punkt musi byc wskazany.
		 * 
		 * @param e
		 *            przekazany przez MouseListener
		 * @return obiekt klasy Point zawierajacy dane o wybranym punkcie
		 */
		private Point searchForPoint(MouseEvent e) {
			int r;
			int c;

			for (int row = 0; row < ROWS - 1; row++) {
				for (int col = 0; col < COLS - 1; col++) {
					if (Math.abs(e.getX() - (getWidth() / COLS * (col + 1))) < PRECISION
							&& Math.abs(e.getY() - (getHeight() / ROWS * (row + 1))) < PRECISION) {
						r = row;
						c = col;
						Point returnPoint = new Point(r, c);
						return returnPoint;
					}
				}
			}
			return null;
		}
		private boolean isCorrect() {
			if (activePoints.equals(code)) {
				return true;
			} else {
				return false;
			}

		}

		//TODO: nie moze byc mozliwosci zaznaczenia punktu ktory nie jest sasiadujacy z poprzednim!! Poprawic.
		public void mouseDragged(MouseEvent e) {
			Point point = searchForPoint(e);
			if (point != null && !activePoints.contains(point)) {
				activePoints.add(point);
				x=e.getX();
				y=e.getY();
				repaint();
				
			}
			x=e.getX();
			y=e.getY();
			repaint();
		}
		
		public void mousePressed(MouseEvent e) {
			x=e.getX();
			y=e.getY();
			Point point = searchForPoint(e);
			if (point != null) {
				activePoints.add(point);
				repaint();
				dragging = true;
			}
		}

		public void mouseReleased(MouseEvent e) {
			
			if (!settingMode) {
				if (activePoints.size() < 4) {
					message.setText("Kod musi skladac sie z ponad 4 punktow");
					activePoints.clear();
					repaint();
				} else {
					if (isCorrect()) {
						message.setForeground(Color.GREEN);
						message.setText("Kod prawidlowy");
						activePoints.clear();
						repaint();
					} else {
						message.setForeground(Color.RED);
						message.setText("Kod nieprawidlowy");
						activePoints.clear();
						repaint();
					}
				}
			}
			
			if (settingMode) {
				if (activePoints.size() < 4) {
					message.setText("Kod musi skladac sie z ponad 4 punktow");
					activePoints.clear();
					repaint();
				} else {
					message.setText("Czy zapisac ten wzor?");
					dragging = false;
					repaint();
				}
			}
			dragging = false;

		}

		public void actionPerformed(ActionEvent e) {
			Object src = e.getSource();
			if (src == setNew) {
				message.setForeground(Color.BLACK);
				message.setText("Wprowadz nowy kod");
				setNew.setEnabled(false);
				confirm.setEnabled(true);
				cancel.setEnabled(true);
				settingMode = true;
			} else if (src == confirm) {
				message.setText("Wprowadz wzor, aby odblokowac");
				setNew.setEnabled(true);
				confirm.setEnabled(false);
				cancel.setEnabled(false);
				settingMode = false;
				//taki konstruktor tworzy kopi� listy activePoints
				code = new ArrayList<Point>(activePoints); 
				activePoints.clear();
				repaint();
			} else if (src == cancel) {
				setNew.setEnabled(true);
				confirm.setEnabled(false);
				cancel.setEnabled(false);
				settingMode = false;
				activePoints.clear();
				repaint();
			}

		}
		public void mouseMoved(MouseEvent e) {
		}

		public void mouseClicked(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}
	}
}
