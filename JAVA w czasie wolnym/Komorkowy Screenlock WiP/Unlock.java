import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.*;


/**
 * Prosty panel do odblokowywania wzorowany na komorkach
 * @author Kuba
 *
 */

public class Unlock {
	
	public static void main(String[] args) {
			
		
		JFrame window = new JFrame("Unlock");
		UnlockPanel content = new UnlockPanel();
		window.setContentPane(content);
		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		window.pack();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocation((screensize.width-window.getWidth())/2, (screensize.height - window.getHeight())/2);
		window.setVisible(true);
	}
	
	

	
}
