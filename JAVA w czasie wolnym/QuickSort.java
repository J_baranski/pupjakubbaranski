/**
 * Klasa służaca próbie napisaniu algorytmu rekursywnego sortowania "quicksort" i porownania go z sortowaniem bąbelkowym odnosnie czasu wykonywania
 * algorytmu przy roznych rozmiarach tablic.
 * 
 * @author Kuba
 *
 */
public class QuickSort {

	/**
	 * pozwala na uruchomienie...
	 */
	public static void main(String[] args) {

		final int LENGTH = 50000;

		int[] array = new int[LENGTH];
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) (Math.random() * 200) + 1;
		}
		int[] arrayClone = array.clone();
		long time = System.currentTimeMillis();
		quickSort(array, 0, array.length - 1);
		long qsTime = System.currentTimeMillis() - time;
		System.out.println("Czas wykonania algorytmu sortowania QuickSort: " + qsTime + " ms");

		time = System.currentTimeMillis();
		bubbleSort(arrayClone);
		long bsTime = System.currentTimeMillis() - time;
		System.out.println("Czas wykonania algorytmu sortowania bąbelkowego: " + bsTime + " ms");
		System.out.println("QuickSort jest o : " + bsTime/qsTime*100 + "% szybszy");

		// for(int i = 0; i < array.length; i++){
		// System.out.println(array[i]);
		// }

	}

	public static void quickSort(int[] array, int low, int high) {

		// base case pozwalajacy na przerwanie rekurencji. Jezeli otrzymana
		// tablica jest jednoelementowa lub pusta.
		if (high <= low) {
			return;
		}

		int i = QuickSortMethod(array, low, high); // zwraca pozycje podzialu
													// tablicy
		quickSort(array, i + 1, high); // stosujemy rekurencyjnie algorytm dla
										// wyzszej tablicy
		quickSort(array, low, i - 1); // a tu dla dolnej czesci

	}

	/**
	 * Metoda bedaca jednym krokiem rekurencyjnym
	 * 
	 * @param int[]
	 *            array : tablica przekazana do metody ktora ma zostac
	 *            posortowana
	 * @param int
	 *            low: dolna granica fragmentu tablicy, ktora ma zostac
	 *            posortowana
	 * @param int
	 *            high: gorna granica fragmentu tablicy, ktora ma zostac
	 *            posortowana.
	 * @return int zwraca pozycję w ktorej zostala umieszczona wartosc p.
	 */
	public static int QuickSortMethod(int[] array, int low, int high) {

		// wybrac jedna wartosc z tablicy ktora bedzie czynnikiem decydujacym o
		// rozdzieleniu pozostalych wartosci na dwie grupy
		// moze byc pierwszy index
		int p = array[low];
		// wartosc p zostala "wyjeta z tablicy" i teraz miejsce po niej jest
		// teoretycznie puste.
		// teraz idac od gory szukamy wartosci, ktora jest mniejsza niz
		// wybrana wartosc p. Wartosc taką przesuwamy na wolne miejsce
		while (high > low) {

			while (high > low && array[high] >= p) {
				high--;
			}
			if (high <= low) {
				break;
			}
			array[low] = array[high]; // teraz array[high] jest miejscem wolnym
			low++;
			// przeniesc wartosc array[high] na wolne miejsce
			while (low < high && array[low] <= p) {
				low++;
			}
			if (high <= low) {
				break;
			}
			array[high] = array[low];
			high--;
		}

		// high i low spotkaly sie i teraz wskazuja na pozostale wolne miejsce.
		// W tym miejscu umieszczamy
		// poczatkowo wyjeta z tabliy. teraz ta wartosc wskazuje miejsce
		// podzialu tablicy na dwie mniejsze.
		// rekurencyjnie stosujemy ten sam algorytm dla powstalych dwoch
		// fragmentow tablicy

		array[high] = p;
		return low;

	}

	public static void bubbleSort(int[] array) {
		int changes = 1;

		while (changes != 0) {
			changes = 0;
			int temp;
			for (int i = 1; i < array.length; i++) {
				if (array[i] < array[i - 1]) {
					temp = array[i];
					array[i] = array[i - 1];
					array[i - 1] = temp;
					changes++;
				}

			}

		}

	}
}
