import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

public class FourInARowPanel extends JPanel {

	public FourInARowPanel() {
		setLayout(null);
		setBackground(new Color(166, 208, 222));
		setPreferredSize(new Dimension(900, 580));

		FourInARowBoard board = new FourInARowBoard();
		add(board);
		add(board.newGameButton);
		add(board.resignButton);
		add(board.message);

		board.setBounds(15, 15, 640, 560);
		board.newGameButton.setBounds(680, 15, 210, 100);
		board.resignButton.setBounds(680, 150, 210, 100);
		board.message.setBounds(680, 300, 210, 100);
	}

}
