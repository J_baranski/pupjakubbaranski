import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class FourInARowBoard extends JPanel implements MouseListener, ActionListener, MouseMotionListener {

	JButton newGameButton;
	JButton resignButton;
	JLabel message;
	int boardData[][];
	int boardFill[];// wskazuje obecn� wysokosc kolumny
	final int EMPTY = 0, RED = 1, YELLOW = 2;
	int currentPlayer;
	boolean gameInProgress;
	int colPointer;

	public FourInARowBoard() {
		addMouseListener(this);
		addMouseMotionListener(this);
		newGameButton = new JButton("NOWA GRA");
		newGameButton.addActionListener(this);
		resignButton = new JButton("PODDAJ SI�!");
		resignButton.addActionListener(this);
		message = new JLabel("Witaj!", JLabel.CENTER);
		message.setFont(new Font("Serif", Font.BOLD, 15));
		setBackground(new Color(47, 165, 204));
		boardData = new int[6][7];
		boardFill = new int[7];
	}

	private void newGame() {
		gameInProgress = true;
		newGameButton.setEnabled(false);
		resignButton.setEnabled(true);
		currentPlayer = RED;
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 7; col++) {
				boardData[row][col] = EMPTY;
			}
		}

		// wyczyscic boardfill
		for (int i = 0; i < boardFill.length; i++) {
			boardFill[i] = 0;
		}
		repaint();
	}

	private void resign() {
		newGameButton.setEnabled(true);
		resignButton.setEnabled(false);
		message.setText("Gracz " + ((currentPlayer == RED) ? "Czerwony" : "��ty") + " podda� si�.");
		gameInProgress = false;
		repaint();
	}

	private void makeMove(int col) {
		// sprawdzic czy kolumna nie jest wypelniona
		if (boardFill[col] == 6) {
			message.setText("Kolumna jest wype�niona. Ruch niedozwolony!");
			return;
		}
		boardData[5 - boardFill[col]][col] = currentPlayer;
		boardFill[col]++;
		repaint();

		if (checkWin(col)) {
			message.setForeground(Color.BLACK);
			message.setText("KONIEC GRY!!!!");
			JOptionPane.showMessageDialog(this,
					"Koniec Gry! Wygrał gracz " + ((currentPlayer == RED) ? "Czerwony." : "Zółty."));
			gameInProgress = false;
		} else {
			if (currentPlayer == RED) {
				currentPlayer = YELLOW;
				message.setForeground(Color.YELLOW);
				message.setText("Ruch gracza ��tego");
			} else {
				currentPlayer = RED;
				message.setForeground(Color.RED);
				message.setText("Ruch gracza czerwonego");
			}
		}
	}

	/**
	 * checks if a move results in victory of a current player
	 * 
	 * @return true if win, false if not
	 */
	private boolean checkWin(int col) {
		// kierunek poziomy
		if (count(col, boardFill, 1, 0) >= 4) {
			return true;
		}
		// kierunek pionowy
		if (count(col, boardFill, 0, 1) >= 4) {
			return true;
		}
		// kierunek skos, prawo
		if (count(col, boardFill, 1, 1) >= 4) {
			return true;
		}
		// kierunek skos, lewo
		if (count(col, boardFill, 1, -1) >= 4) {
			return true;
		}
		return false;
	}

	/**
	 * Counts currentPlayer tiles in direction given in argument, czy jakos tak
	 * 
	
	 */
	private int count(int col, int boardFill[], int dirX, int dirY) {
		// zakladany ze obecnie wstawiony krazek jest poprawny

		int row = 6 - boardFill[col];
		int r = row + dirY; // wiersz przesuniety o wartosc dirY z
							// argumentu
		int c = col + dirX;
		int count = 1;
		// sprawdzac kolejne krazki w danym kierunku tak dlugo az analizowany
		// krazek bedzie nalezal do obecnego gracza
		// i nie wyjdziemy poza plansze
		while (r >= 0 && r < 6 && c >= 0 && c < 7 && boardData[r][c] == currentPlayer) {
			count++;
			r += dirY;
			c += dirX;
		}

		// i teraz w drugą strone od zadanego krazka

		r = row - dirY;
		c = col - dirX;

		while (r >= 0 && r < 6 && c >= 0 && c < 7 && boardData[r][c] == currentPlayer) {
			count++;
			r -= dirY;
			c -= dirX;
		}
		return count;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// wygladzic
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
		g.drawRect(1, 1, getWidth() - 3, getHeight() - 3);

		// puste pola
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 7; col++) {
				g.setColor(Color.WHITE);
				g.fillOval(80 * col + 10 * (col + 1), 80 * row + 10 * (row + 1), 80, 80);
				g.setColor(Color.BLACK);
				g.drawOval(80 * col + 10 * (col + 1), 80 * row + 10 * (row + 1), 80, 80);
				g.drawOval(80 * col + 10 * (col + 1), 80 * row + 10 * (row + 1), 79, 80);
				g.drawOval(80 * col + 10 * (col + 1), 80 * row + 10 * (row + 1), 78, 80);
			}
		}

		// wype�nic pola kolorowymi kr��kami na podstawie stanu gry
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 7; col++) {
				if (boardData[row][col] == RED) {
					g.setColor(Color.RED);
					g.fillOval(80 * col + 10 * (col + 1) + 1, 80 * row + 10 * (row + 1) + 1, 79, 79);
				}
				if (boardData[row][col] == YELLOW) {
					g.setColor(Color.YELLOW);
					g.fillOval(80 * col + 10 * (col + 1) + 1, 80 * row + 10 * (row + 1) + 1, 79, 79);
				}
			}
		}

		// ramka wskazujaca kolumne nad ktora znajduje sie wskaznik
		if (gameInProgress) {
			g.setColor(Color.DARK_GRAY);
			g.drawRect(5 + colPointer * 90, 5, 90, 550);
			g.drawRect(6 + colPointer * 90, 6, 88, 548);
		}
	}

	public void mouseClicked(MouseEvent e) {
		if (gameInProgress) {
			int x = e.getX();
			int col = +(x - 5) / 90;
			if (col >= 0 && col < 8) {
				makeMove(col);
			}
		} else {
			message.setText("Rozpocznij now� gr�!");
		}
	}

	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		if (src == newGameButton) {
			newGame();
		} else if (src == resignButton) {
			resign();
		}
	}

	public void mouseMoved(MouseEvent e) {
		int x = e.getX();
		int col = +(x - 5) / 90;
		if (col >= 0 && col < 8) {
			colPointer = col;
			repaint();
		}
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
	}

	public void mouseReleased(MouseEvent arg0) {
	}

	public void mouseDragged(MouseEvent arg0) {
	}
}
