import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class FourInARow extends JFrame {
	
	public static void main(String[] args) {
		
		FourInARow window = new FourInARow();
		FourInARowPanel content = new FourInARowPanel();
		window.setContentPane(content);
		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		window.pack();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocation((screensize.width - window.getWidth())/2, (screensize.height - window.getHeight())/2);
		window.setResizable(false);
		window.setVisible(true);
		
		
		
	}
}
