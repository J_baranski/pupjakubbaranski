/**
* Doswiadczenie z rekurencja
*
*/
public class Hanoi {
	public static void main(String[] args) {
		int result = towersOfHanoi(15, 1, 2, 3);
		System.out.println(result + ": liczba koniecznych do wykonania ruch�w");
		System.out.println();
		System.out.println("Zgodnie z zasad� oryginaln� zasad� legendy o tybeta�skich mnichach");
		System.out.println("pozwalaj�c� na przeniesienie jednego kr��ka dziennie");
		System.out.println("rozwi�zanie tej zagadki zaj�o by oko�o: "+ ((result/365 > 1)?(result/365 +" lat(a) i "+result%365) : (result%365))+" dni");
		
	}

	static int towersOfHanoi(int N, int from, int to, int spare) {
		int count = 1;
		if (N == 1) {
			System.out.println("Przenie� dysk z pola " + from + " na pole " + to);
		} else {
			count+=towersOfHanoi(N - 1, from, spare, to);
			System.out.println("Przenie� dysk z pola " + from + " na pole " + to);
			count+=towersOfHanoi(N - 1, spare, to, from);
		}
		return count;
	}
}
