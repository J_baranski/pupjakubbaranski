<!DOCTYPE html>

<html>
    <head>
        <title>Tajtel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="stylesheet.css"/>
        <link href="https://fonts.googleapis.com/css?family=Amatic+SC" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
        <script type="text/javascript" src="javascript1.js"></script>

    </head>
    <body>

        <?php
        session_start();
        echo 'session started';
        if ($_SESSION[user][login] == NULL) {
            echo '<br/><br/><a onclick="blackout()">Zaloguj</a><br/>'
            . '<a href="register.php">Zarejestruj</a>';
        } else {
            echo'<br/><br/><a href="do_loginout.php">Wyloguj</a>';
        }
        echo $_SESSION[user][login];
        ?>

        <img id="logoimg" src="images/icons/home.png" alt=""/>
        <div class="dropdown">
            <img id="menu-icon" src="images/icons/mobilemenu.png" alt="menu-icon"/>
            <div class="dropdown-content">
                <ul>
                    <li><a href="#link">HOME</a> </li>
                    <li><a href="#link">CONTACT</a> </li>
                    <li><a href="#link">APPS</a> </li>
                    <li><a href="#link">PLACEHOLDER</a> </li>
                </ul>
            </div>
        </div>


        <nav class="col-xs-12 nav">
            <div class="col-md-3 col-xs-6 button active"><a href="#link"><span><strong>HOME</strong></span></a></div>
            <div class="col-md-3 col-xs-6 button"><a href="#link"><span>CONTACT</span></a></div>
            <div class="col-md-3 col-xs-6 button"><a href="#link"><span>APPS</span></a></div>
            <div class="col-md-3 col-xs-6 button"><a href="#link"><span>PLACEHOLDER</span></a></div>
        </nav>
        <div class="blackout" onclick="blackoutend()"></div>


        <div class="col-xs-12 main"> 

            <div class="login">
                <span>
                    <?php
                    require './login_form.php';
                    ?>
                </span>
            </div>

            <div class="bg dark_bg">
                <div class="section_content">
                    <h1>BIOTECHNOLOGIST;</h1><h1>BEGINNER PROGRAMMER;</h1>
                    <p>because the diversity of interests is not a hinderance.</p>
                </div>
            </div>
            <div class="bg white_bg white_bg">
                <div class="section_content">
                    <h2>My (very) short story</h2>
                    <p>Even though I studied how to make a living cell do my bidding, computers are not that different after all. Programming and coding has always been something I enjoyed doing as a hobby. Hopefully it can become something more.</p>
                </div>

            </div>
            <div class="bg gradient_bg">
                <div class='section_content'>
                    <h2>EXPERIENCE</h2>
                    <p>Well, since I'm starting, this section is temporarly just a placeholder. Hopefully you can help me change it!</p>

                </div>
            </div>
            <div class="bg dark_bg">
                <div class="section_content">
                    <h2>Skills</h2>
                    <p></p>
                </div>

            </div>
            <footer>
                Projekt i Wykonanie: Jakub Barański <br/>
                2016
                Wszelkie prawa zastrzeżone<br/>
                <a href=mailto:jak.bar92@gmail.com>Napisz!</a>
            </footer>



        </div>
    </div>
    <?php exit; ?>
</body>
</html>
