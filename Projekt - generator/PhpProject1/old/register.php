<!DOCTYPE html>

<html>
    <head>
        <title>Tajtel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="stylesheet.css"/>
        <link href="https://fonts.googleapis.com/css?family=Amatic+SC" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
        <script type="text/javascript" src="javascript1.js"></script>

    </head>
    <body>
        
        
        <div id="document">
            <header>
                <?php
                session_start();
                echo 'session started'; 
                if ($_SESSION[user][login]== NULL){
                     echo '<br/><br/><a onclick="blackout()">Zaloguj</a><br/>'
                    . '<a href="register.php">Zarejestruj</a>';
                }else {
                    echo'<br/><br/><a href="do_loginout.php">Wyloguj</a>';
                    }
                echo $_SESSION[user][login];
                    ?>
                
               
                <span id="logo">Jakub Barański<br/>

                    Website</span>
                
                <img id="logoimg" src="images/icons/home.png" alt=""/>
                <div class="dropdown">
                    <img id="menu-icon" src="images/icons/mobilemenu.png" alt="menu-icon"/>
                    <div class="dropdown-content">
                        <ul>
                            <li><a href="#link">HOME</a> </li>
                            <li><a href="#link">CONTACT</a> </li>
                            <li><a href="#link">APPS</a> </li>
                            <li><a href="#link">PLACEHOLDER</a> </li>
                        </ul>
                    </div>
                </div>


                <div id="nav" class="col-xs-12">
                    <div class="col-md-3 col-xs-6 button active"><a href="#link"><span><strong>HOME</strong></span></a></div>
                    <div class="col-md-3 col-xs-6 button"><a href="#link"><span>CONTACT</span></a></div>
                    <div class="col-md-3 col-xs-6 button"><a href="#link"><span>APPS</span></a></div>
                    <div class="col-md-3 col-xs-6 button"><a href="#link"><span>PLACEHOLDER</span></a></div>
                </div>

            </header>
            <div id="blackout" onclick="blackoutend()"></div>


            <div class="row" id="main"> 

                <div id="login">
                    <span>
                    <?php
                    require './login_form.php';
                    ?>
                    </span>
                </div>
                <?php
                    require_once './register_form.php';
                ?>
            </div>
                
                <footer>
                    Projekt i Wykonanie: Jakub Barański <br/>
                    2016
                    Wszelkie prawa zastrzeżone<br/>
                    <a href=mailto:jak.bar92@gmail.com>Napisz!</a>
                </footer>



            </div>
            <?php exit; ?>
    </body>
</html>


