$(document).ready(function () {


var barReveal = {
  origin : 'left',
  delay    : 200,
  duration: 400,
  distance : '1000px',
  easing   : 'ease-in-out',
};

var mainSecReveal = {
    origin: 'right',
    duration: 700,
    scale: 50,
    opacity: 0
};


window.sr = ScrollReveal();
sr.reveal('.progress', barReveal);
sr.reveal('.main_section', mainSecReveal);
sr.reveal('.story');
sr.reveal('.experience');
});


