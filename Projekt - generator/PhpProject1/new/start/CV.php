
<div  class="container-fluid my_cv_container">
    <div class="bg bg_light col-xs-12 bg_main">
        <div class="section_content main_section">
            <h1><strong>BIOTECHNOLOGIST;<br/>BEGINNER PROGRAMMER;</strong></h1>
            <p>because the diversity of interests is not a hinderance.</p>
        </div>
    </div>
    <div class="bg bg_dark col-xs-12">
        <div class="section_content story">
            <h2>My (very) short story</h2>
            <p>Even though I studied how to make a living cell do my bidding, computers are not that different after all. Programming and coding has been my hobby for quite some time. Hopefully it can become something more.</p>
        </div>

    </div>
    <div class="bg bg_light col-xs-12">
        <div class='section_content experience'>
            <h2>EXPERIENCE</h2>
            <p>Well, since I'm starting, this section is temporarly just a placeholder. Hopefully you can help me change it!</p>

        </div>
    </div>
    <div class="bg bg_dark col-xs-12 skills ">
        <div class="section_content">
            <h2>Skills</h2>
            <div class="prograss_col col-xs-12 col-md-6">
                <p>JavaScript</p>
                <div class="progress">
                    <div class="progress-bar my_progress_bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                        <span class="sr-only">45% Complete</span>
                    </div>
                </div>
            </div>
            <div class="prograss_col col-xs-12 col-md-6">
                <p>Django</p>
                <div class="progress">
                    <div class="progress-bar my_progress_bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                        <span class="sr-only">45% Complete</span>
                    </div>
                </div>
            </div>
            <div class="prograss_col col-xs-12 col-md-6">
                <p>JAVA</p>
                <div class="progress">
                    <div class="progress-bar my_progress_bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 55%">
                        <span class="sr-only">45% Complete</span>
                    </div>
                </div>
            </div>
            <div class="prograss_col col-xs-12 col-md-6">
                <p>HTML/CSS</p>
                <div class="progress">
                    <div class="progress-bar my_progress_bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                        <span class="sr-only">45% Complete</span>
                    </div>
                </div>
            </div>




        </div>

    </div>
</div>
