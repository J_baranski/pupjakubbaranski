$(document).ready(function(){
	$('button').on('click', function(){
		var imie = $('#imie').val();
		var nazwisko = $('#nazwisko').val();
		var action = $(this).attr('id');
		
		$.ajax({
			url: 'json.php',
			dataType: 'json',
			//pozwala na przeslanie pewnych rzeczy,, np pol formularza
			data: {imie: imie, nazwisko: nazwisko, action: action },
			
			//po pobraniu informacji od backendu bedą się dziać rzeczy z successu!
			success: function(data){ 
				if(action == 'save' && data.status == 'ok'){
					$('.info').text('Zapisano').css('color', 'green');
				}
				else if(action == 'show' && data.status == 'ok'){
					$('.info').text(' ');
					var users = data.users;
					$('<ul/>', {'class': 'list'}).appendTo('.info');
					for(var i = 0 ; i< users.length-1; i++){
						var temp = users[i].split('|');
						$('<li/>').text("Imie: "+temp[0]+"  Nazwisko: "+temp[1]).css('color', 'black').appendTo('.list');
						
					}
				}
			}
				
			
		});	
	})
});

//ajax umozliwia pobranie informacji z backendu bez koniecznosci przeladowywania strony!
