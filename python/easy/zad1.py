# -*- coding: utf-8 -*-

import sys
import random

def pot ():
	while True:
		try:
			a = raw_input ('podaj pierwsza liczbe   ')
			b = raw_input ('teraz druga   ')
		except KeyboardInterrupt:
			input = raw_input("\n\nNa pewno chcesz wyjsc? [t/n]").lower()
			if input.startswith('t'):
				sys.exit('nara')
		try:
			a = float(a)
			b = float(b)
			out = a**b
			return out
		#except OverflowError as detail:
		#	print "Liczby poza zasiegiem. Kompilator placze: ", detail, "\n\n"
		#except ValueError as detail:
		#	print "Podane liczby powiny byc numerami, Kompilator placze: ", detail, "\n\n"	
		except:	
			print str(a) + " ^ " + str(b) + "\n\n"
		#finally:
		#	print "wprowadzono dzialanie: ", str(a) + " ^ " + str(b)
		
def isNumber():
	a = raw_input('?   ')
	if a.startswith('-'):
		a = a[1:]
		
	if not a.isdigit():
		return "Error: ", a, "is a string"
	else:
		return True;		

# print '{:.5f}'.format(pot())	
# print(isNumber())

## symulacja papier kamien nozyce

def games():
	PAPER = 1
	ROCK = 2
	SCISSORS = 3
	while True:
		try:
			GAMES = int(raw_input("How many games would you like to play?     "))			
			break
		except:
			print "Wrong input. Exiting"
			return
	
	options = [PAPER, ROCK, SCISSORS]
	
	for i in range (0, GAMES):
		comp = random.choice(options)
		print comp
		while True:			
			try:
				player = int(raw_input("PAPER: 1; ROCK: 2; SCISSORS: 3 \n\n"))
				if player < 1 or player > 3:
					raise
				break;
			except:
				print "Wrong input. Try again"
		print "Your choice: " , trans(player) , "\n"
		print "Computer chose ", trans(comp)
		if checkWin(comp, player) == -1 :
			print "Tie!"
		elif checkWin(comp, player):
			print "You won!"
		else:
			print "Computer won!"
		
def trans(i=4):
	if i ==	1:
		return "PAPER"
	if i == 2:
		return "ROCK"
	if i == 3:
		return "SCISSORS"
	if i == 4:
		return "SPOCK"
	
def checkWin(comp, player=4):
	if(comp == player):
		return -1
	elif comp == 0:
		if player == 1:
			return False
		elif player == 2:
			return True
	elif comp == 1:
		if player == 0:
			return True
		elif player == 2:
			return False
	elif comp == 2:
		if player == 0:
			return False
		elif player == 1:
			return True
	elif player == 4:
		return True
		
a = games()

