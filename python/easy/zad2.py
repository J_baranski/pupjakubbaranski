def strength():
	passw = raw_input('Input password: ')
	str = 0;	
	stats = {'digits' : 0, 'lowerCase':0, 'upperCase':0, 'other': 0}
	for i in passw:
		if i.isdigit():
			str += 3
			stats['digits'] += 1
		elif i.isalpha() and i == i.lower():
			str +=1
			stats['lowerCase'] += 1
		elif i.isalpha():
			str += 2
			stats['upperCase'] += 1
		else:
			str += 4
			stats['other'] += 1
	print "Password strength rated: " , str
	print "Used:\n", stats['digits'], " digits"
	print stats['lowerCase'], " lower case alphabetic characters"
	print stats['upperCase'], " upper case alphabetic characters"
	print stats['other'], " special characters"
	
strength()
