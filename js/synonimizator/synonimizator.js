var str = "{Paweł|Marek|Stefan|Eustachy|Eugeniusz} {    poszedł|pobiegł|zaszedł|popędził} do {sklepu|domu|lasu|szkoły|babci|cioci|kochanki|żony|Adama|Ppzyjaciela}. {Pomimo|Mimo|Bez} {strachu|bólu|wątpliwości}, dążył {śmiało|niepewnie|uparcie|pewnie|lękliwie} do {celu|przeznaczenia}, mając u boku {dzielnego|strachliwego|głupiego} {towarzysza|kompana} {podróży|wyprawy|wędrówki} imieniem {Edward|Marcin|Ludwik}. \n\n";

function Synonimizer(str) {
    this.str = str;
    var delimiter = '|';

    this.processInner = function () {
        var outS = '';
        var open = this.str.indexOf('{');
        if(open === -1) return false;
        var nextOpen = this.str.indexOf('{', open + 1);
        var close = this.str.indexOf('}');
            while (nextOpen < close && nextOpen !== -1 && close !== -1) {
                open = nextOpen;
                nextOpen = this.str.indexOf('{', open + 1);
            }
        outS = this.str.substring(0, open) + this.chooseWord(this.str.substring(open + 1, close)) + this.str.substring(close + 1);
        this.str = outS;
        return true;
    };
    
    this.chooseWord = function (str) {
        var array = str.split(delimiter);
        for (var i in array) {
            array[i] = array[i].trim();
        }
        return array[Math.floor(Math.random() * array.length)];
    };

    this.setDelimiter = function (d) {
        this.delimiter = d;
    };
    
    this.processAll = function(){
        while(true){
            if(this.processInner() === false) break;
        }
    };
    
    this.getString = function(){
        return this.str;
    };
}

var re = new Synonimizer(str);
re.processAll();
console.log(re.getString());