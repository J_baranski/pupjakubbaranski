function Stack() {

    var array = [];

    function empty() {
        if (array.length === 0)
            return true;
        else
            return false;
    }
    ;

    function pop() {
        if (!empty())
            return array.pop();
        else
            return null;
    }
    ;

    function push(item) {
        array.push(item);
    }
    ;

    function size() {
        return array.length;
    }
    ;

    function top() {
        if (!empty())
            return array.length - 1;
        else
            return null;
    }
    ;

    return{
        empty: empty,
        pop: pop,
        push: push,
        size: size,
        top: top
    };
}

var stack2 = (function () {
    var array = [];

    function empty() {
        if (array.length === 0)
            return true;
        else
            return false;
    }

    function pop() {
        if (empty() !== true)
            return array.pop();
        else
            return null;
    }

    function push(item) {
        array.push(item);
    }

    function size() {
        return array.length;
    }

    function top() {
        if (!empty())
            return array.length - 1;
        else
            return null;
    }
    return{
        empty: empty,
        pop: pop,
        push: push,
        size: size,
        top: top
    };
})();

function Queue(s) {
    this.elems = [];
    this.maxSize = s || 100;
}

Queue.prototype.empty = function () {
    return (this.elems.length === 0) ? true : false;
};

Queue.prototype.pop = function () {
    if (this.size() === 1) {
        this.head = null;
        this.tail = null;
    }
    return (!this.empty()) ? this.elems.shift() : null;
};

Queue.prototype.push = function (arg) {
    if (this.empty()) {
        this.head = 0;
        this.tail = this.elems.length + 1;
    }
    if (this.elems.length === this.maxSize)
        throw "Za duzo tych elementow już...";
    this.elems.push(arg);
};

Queue.prototype.size = function () {
    return this.elems.length;
};

Queue.prototype.head = null;
Queue.prototype.tail = null;


function ONP(s) {
    var exp = s.split(' ');
    var stack = new Stack();
    var operands = ['+', '-', '*', '/', '%', '^'];
    var op;
    for (var i in exp) {
        if (operands.indexOf(exp[i]) !== -1) {
            try {
                op = exp[i];
                var factorTwo = Number(stack.pop());
                var factorOne = Number(stack.pop());
                stack.push(proc(factorOne, factorTwo, op));
            } catch (err) {
                return err;
            }

        } else {
            ;
            stack.push(exp[i]);
        }
    }
    return stack.pop();
}

function NP(s) {
    var exp = s.split(' ');
    var opStack = new Stack();
    var numStack = new Stack();
    var operands = ['+', '-', '*', '/', '%', '^'];
    var op;
    var factorOne;
    var factorTwo;

    for (var i in exp) {
        if (operands.indexOf(exp[i]) !== -1)
            opStack.push(exp[i]);
        else
            numStack.push(exp[i]);
    }
    while (!opStack.empty()) {
        factorTwo = Number(numStack.pop());
        factorOne = Number(numStack.pop());
        op = opStack.pop();
        numStack.push(proc(factorOne, factorTwo, op));
    }
    return numStack.pop();
}

function proc(factorOne, factorTwo, op) {
    switch (op) {
        case '+':
            return factorOne + factorTwo;
        case '-':
            return factorOne - factorTwo;
        case '*':
            return factorOne * factorTwo;
        case '/':
            if (factorTwo === 0)
                throw "Err: Trying to divide by 0";
            return factorOne / factorTwo;
        case '%':
            return factorOne % factorTwo;
        case '^':
            return Math.pow(factorOne, factorTwo);
    }
}

function isPalindrome(s) {
    var stack = new Stack();
    var reversed = '';
    for (var i in s) {
        stack.push(s[i]);
    }
    for (var i in s) {
        reversed += stack.pop()
    }

    if (s == reversed)
        return true;
    else
        return false;
}


function firstNonRepeatingLetter(s) {
    var lowS = s.toLowerCase();
    var present = [];

    for (var i in lowS) {
        if (lowS.indexOf(lowS[i], +i + 1) === -1 && present.indexOf(lowS[i]) === -1)
            return s[i];
        present.push(lowS[i]);
    }
    return '';
}

var List = function (maxS) {
    var array = [];
    var maxSize = maxS || 100;

    this.push_front = function (s) {
        if(array.length-1<maxSize) array.unshift(s);
        else throw "List max size reached";
    };
    this.push_back = function (s) {
        if(array.length-1<maxSize){array.push(s) ;} 
        else  throw "List max size reached";
    };
    this.insert = function (i, item) {
        array.splice(i, 0, item);
    };
    this.pop_front = function () {
        return (array.length !== 0) ? array.shift() : null;
    };
    this.pop_back = function () {
        return (array.length !== 0) ? array.pop() : null;
    };
    this.size = function () {
        return array.length;
    };
    this.max_size = function () {
        return maxSize;
    };
    this.empty = function () {
        return (array.length === 0) ? true : false;
    };
    this.remove = function (v) {
        for (var i = 0; i<array.length; ++i) {
            if (array[i] === v){
                
                array.splice(i, 1);
                i--;
            }
            
        }
    };
    this.sort = function () {
        var minI;
        var temp;
        for (var i = 0; i < array.length; i++) {
            minI = i;
            for (var j = i + 1; j < array.length; j++) {
                if (array[j] < array[i])
                    minI = j;
            }
            if (minI !== i) {
                temp = array[i];
                array[i] = array[minI];
                array[minI] = temp;
            }
        }
    };


    this.reverse = function () {
        var temp = [];
        for (var i = array.length - 1; i >= 0; --i) {
            array[i] = temp[array.length - 1 - i];
        }
        array = temp;
    };

};

var list = new List();
list.push_back(3);
list.push_front(12);
list.push_back(2);
console.log(list.size());
list.sort();
console.log(list.pop_back());
console.log(list.pop_back());
console.log(list.size());
