
$(document).ready(function () {
   /*Tworzenie tabeli*/ 
    $('<table/>', {class: 'table'}).appendTo('.container');
    for (var i = 0; i < 3; i++) {
        $('<tr/>').appendTo('.table');

    }
    for (var i = 0; i < 3; i++) {
        $('<td/>', {style: 'border: solid black 1px; display: inline-block', width: '50px', height: '50px'}).appendTo('tr')[i];

    }
    $('<div/>', {class: 'message'}).appendTo('.container');
    var td = $('td');
    /*Generowanie tablicy [][] zawierajacej komorki tabeli*/
    data = [];
    for (var i = 0; i < 3; i++) {
        data[i] = [];
        for (var j = 0; j < 3; j++) {
            data[i].push(td[j + i * 3]);
        }
    }
    /*Stałe odpowiadajace za rozroznianie graczy*/
    var CROSS = 'X',
        CIRCLE = 'O';
    var currentPlayer = CROSS;

    /*do wariantu dla jednego gracza;*/
    var player = CROSS;
    var comp = CIRCLE;


    $('.message').text('currentPlayer: ' + currentPlayer);

/*
    LISTENER DLA DWOCH GRACZY
    td.on('click', function () {
        $(this).off('click');
        $(this).text(currentPlayer);
        if (checkWin()) {
            td.off('click');
            $('.message').text('Wygrał ' + currentPlayer);
        } else {
            currentPlayer = ((currentPlayer === CROSS) ? CIRCLE : CROSS);
            $('.message').text('currentPlayer: ' + currentPlayer);
        }
    });
*/


/*LISTENER DLA JEDNEGO GRACZA*/
    td.on('click', function () {
        var target = $(this);
        $(this).off('click');
        if($(this).text() == '')$(this).text(player);
        else return;
        if (checkWin()) {
            td.off('click');
            $('.message').text('Wygrał ' + currentPlayer);
            return;
        } else {

            currentPlayer = comp;
            compMove();
            if (checkWin()) {
                td.off('click');
                $('.message').text('Wygrał ' + currentPlayer);
            }
            currentPlayer = player;

        }
    });


/**
 * Funkcja sprawdza czy na planszy znajduje sie zwycieska sekwencja ktoregokolwiek z graczy
 * Function checks if board contains a winning sequence belonging to either of players
 * 
 * @returns true if win, false if not
 */
    function checkWin() {

        var win;
        /*sprawdzic rzedy*/
        for (var i = 0; i < 3; i++) {
            /*zalozyc wygrana*/
            win = true;
            for (var j = 0; j < 3; j++) {
                /*jezeli nie zgadza sie z obecnym graczem, zmienic na false (i wyskoczyc z petli?)*/
                if (data[i][j].innerText !== currentPlayer)
                    win = false;
            }
            if (win)
                return win;
        }
        /*sprawdzic kolumny*/
        for (var j = 0; j < 3; j++) {
            win = true;
            for (var i = 0; i < 3; i++) {
                if (data[i][j].innerText !== currentPlayer)
                    win = false;
            }
            if (win)
                return win;
        }
        /*sprawdzic skosy*/
        win = true;
        for (var i = 0; i < 3; i++) {
            if (data[i][i].innerText !== currentPlayer)
                win = false;
        }
        if (win)
            return win;
        win = true;
        for (var i = 0; i < 3; i++) {
            if (data[i][2 - i].innerText !== currentPlayer)
                win = false;
        }
        if (win)
            return win;
        return false;
    }


/*  
    algorytm sztucznej inteligencji
    jezeli wystepuja w rzedze/kolumnie/skosie dwa symbole komputera, wstaw tam swoj.
    inaczej jezeli wystepuja w rzedzie/kolumnie/skosie dwa symbole gracza, wstaw tam swoj
    inaczej: wybierz pole losowo
    funkcja bedzie sama wstawiac symbol komputera
*/

    
/*sprawdza ruch korzystny dla siebie. nalezy dodac jeszcze cos, zeby w razie braku mozliwosci wygranej
przeszkadzał graczowi przeciwnemu.*/
    function compMove() {
        var count;
        var free;
        /*sprawdzic rzedy*/
        for (var i = 0; i < 3; i++) {
            count = 0;
            for (var j = 0; j < 3; j++) {
                if (data[i][j].innerText === comp)
                    count++;
                else
                    free = j;
            }
            /*podejmij decyzje. Jezeli wystepuja dwa symbole komputera, wstaw symbol w wolne miejsce*/
            if (count === 2) {
                /*wstaw symbol na pozycje data[i][free]*/
                if (data[i][free].innerText === '') {
                    data[i][free].innerText = comp;
                    return;
                }
            }
        }

        /*sprawdzic kolumny*/
        for (var j = 0; j < 3; j++) {
            count = 0;
            free = 0;
            for (var i = 0; i < 3; i++) {
                if (data[i][j].innerText === comp)
                    count++;
                else
                    free = i;
            }
            /*podejmij decyzje. Jezeli wystepuja dwa symbole komputera, wstaw symbol w wolne miejsce*/
            if (count === 2) {
                /*wstaw symbol na pozycje data[i][free]*/
                if (data[free][j].innerText === '') {
                    data[free][j].innerText = comp;
                    return;
                }
            }
        }

        /*sprawdzic skosy (1)*/
        count = 0;
        free = 0;
        for (var i = 0; i < 3; i++) {
            if (data[i][i].innerText === comp)
                count++;
            else
                free = i;
        }
        /*podejmij decyzje. Jezeli wystepuja dwa symbole komputera, wstaw symbol w wolne miejsce*/
        if (count === 2) {
            /*wstaw symbol na pozycje data[i][free]*/
            if (data[free][free].innerText === '') {
                data[free][free].innerText = comp;
                return;
            }
        }
        /*sprawdzic skosy (2)*/
        count = 0;
        free = 0;
        for (var i = 0; i < 3; i++) {
            if (data[i][2 - i].innerText === comp)
                count++;
            else
                free = i;
        }
        /*podejmij decyzje. Jezeli wystepuja dwa symbole komputera, wstaw symbol w wolne miejsce*/
        if (count === 2) {
            /*wstaw symbol na pozycje data[i][free]*/
            if (data[free][2 - free].innerText === '') {
                data[free][2 - free].innerText = comp;
                return;
            }
        }

        /*jezeli zaden z powyzszych warunkow nie jest spelniony, probuj wstawic losowa, tak dlugo jak wybrane pole nie jest zajete*/
        do {
            randI = Math.floor(Math.random() * 3);
            randJ = Math.floor(Math.random() * 3);
            console.log(randI, randJ);
        } while (data[randI][randJ].innerText !== '')

        data[randI][randJ].innerText = comp;
        console.log(data[randI][randJ]);
    }
});



