function Vehicle(vehicleType) {
    this.vehicleType = vehicleType || 'car';
    this.model = "default";
    this.licence = "00000000";
}

var testInstance = new Vehicle("Mercedes");
//console.log(testInstance);

/* Wzorzec dekoratora - rozszerzanie instancji obiektu. W innych jezykach programowania wzorzec ten wyglada i zachowuje sie inaczej*/

var lada = new Vehicle("Łada");
lada.setModel = function (modelName) {
    this.model = modelName;
};

lada.setColor = function (color) {
    this.color = color;
};
/* te funkcje teraz beda dostepne tylko dla tej jednej instancji tego obiektu*/
lada.setModel("314");
lada.setColor("red");
//
//console.log(lada);
//
//var testInstanceTwo = new Vehicle("Porsche");
//console.log(testInstanceTwo);

/*Drugi sposob, bardzije zblizony do implementacji w innych jezykach programowania*/

function Laptop() {
    this.cost = function () {
        return 2000;
    };
    this.screenSize = function () {
        return 14.1;
    };
    this.screenType = function () {
        return "default";
    };
    this.screenSurface = function () {
        return "default";
    };
}

/*dekorator nr 1*/
function memory(laptop) {
    var price = laptop.cost();
    laptop.cost = function () {
        return price + 150;
    };
}

/* dekorator nr 2*/
function graphicCard(laptop) {
    var price = laptop.cost();
    laptop.cost = function () {
        return price + 300;
    };
}
/*dekorator nr 3*/
function hardDrive(laptop) {
    var price = laptop.cost();
    laptop.cost = function () {
        return price + 1000;
    };
}

var myLaptop = new Laptop();
//memory(myLaptop);
//console.log(myLaptop.cost());
//
//graphicCard(myLaptop);
//console.log(myLaptop.cost());
//
//hardDrive(myLaptop);
//console.log(myLaptop.cost());

//dodac screenType, screenSurface;

function screenLED(laptop) {
    var price = laptop.cost();
    laptop.screenType = function () {
        return "LED";
    };
    laptop.cost = function () {
        return price + 100;
    }
}
function screenMatte(laptop) {
    var price = laptop.cost();
    laptop.cost = function () {
        return price + 330;
    }
    laptop.screenSurface = function () {
        return "Matte";
    };
}
//console.log(myLaptop.screenSurface(), myLaptop.cost());
//screenMatte(myLaptop);
//console.log(myLaptop.screenType() ,myLaptop.screenSurface(), myLaptop.cost());
//screenLED(myLaptop);
//console.log(myLaptop.screenType() ,myLaptop.screenSurface(), myLaptop.cost());

/*fabryka dekoratorów*/

function decorateLaptop(laptop, component) {
    var price = laptop.cost();
    switch (component) {

        case "graphicCard":
        {
            laptop.cost = function () {
                return price + 1000;
            };
            break;
        }
        case "memory":
        {
            laptop.cost = function () {
                return price + 100;
            };
            break;
        }
        case "hardDrive":
        {
            laptop.cost = function () {
                return price + 500;
            };
            break;
        }
        case "screenMatte":
        {
            laptop.screenSurface = function () {
                return "Matte";
            };
            break;
        }

    }
}
;
//console.log(myLaptop.cost());
//decorateLaptop(myLaptop, "graphicCard");
//console.log(myLaptop.cost());
//decorateLaptop(myLaptop, 'hardDrive');
//console.log(myLaptop.cost());



//function decorateLaptop(laptop, component, CompPrice) {
//
//    laptop.cost = function () {
//        return price + CompPrice;
//    };
//
//    laptop.components = laptop.components || [];
//    laptop.components.push(component);
//
//}
//;



function decorateLaptop(laptop, component, CompPrice) {
    var price = laptop.cost();
    switch (component) {

        case "graphicCard":
        {
            laptop.components = laptop.components || [];
            laptop.components.push("Graphic Card");

            laptop.cost = function () {
                return price + parseFloat(CompPrice);
            };
            break;
        }
        case "memory":
        {
            laptop.components = laptop.components || [];
            laptop.components.push("Memory");

            laptop.cost = function () {
                return price + parseFloat(CompPrice);
            };
            break;
        }
        case "hardDrive":
        {
            laptop.components = laptop.components || [];
            laptop.components.push('Hard Drive');

            laptop.cost = function () {
                return price + parseFloat(CompPrice);
            };
            break;
        }
    }
}
;

console.log(myLaptop.cost(), myLaptop.components);
decorateLaptop(myLaptop, "graphicCard", 800);
console.log(myLaptop.cost(), "Components list: " + myLaptop.components);
decorateLaptop(myLaptop, "memory", 100);
console.log(myLaptop.cost(), "Components list: " + myLaptop.components);