/*Singleton wymusza wywołanie jednej i tej samej instancji obiektu przez cały czas działania skryptu*/

var singleton = (function () {
    var instance;

    function init() {
        function privateMethod() {
            console.log("privateMethod");
        }

        var privateVariable = "privateVariable";
        var privateRandomNum = Math.random();

        return {
            publicMethod: function () {
                console.log("publicMethod");
            },
            publicProperty: "publicProperty",
            getRandomNum: function () {
                return privateRandomNum;
            }
        };
    }

    return{
        getInstance: function () {
            /*Lazy initialization.*/
            if (!instance) {
                instance = init();

            }
            return instance;
            /*Zawsze bedzie zwracana jedna instancja obiektu. Obiekty sa inne, 
             * ale przypisywana jest im ta sama instancja obiektu*/
        }
    };
})();

var captain = (function () {
    var instance;

    function init() {
        var privateCaptain = "Kuba B.";
        
        return{
            getCaptain: function () {
                return privateCaptain;
            }
        };
    }
    return{
        getInstance: function () {
            if (!instance)
                instance = init();
            return instance;
        }
    };
})();

