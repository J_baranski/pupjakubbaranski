function Car(model, year, mileage) {
    this.model = model;
    this.year = year;
    this.mileage = mileage;
    
    this.toString = function(){
        return this.model + "("+this.year+") passed "+this.mileage +" km.";
    };
}



function Figure (type, area, circumference){
    this.type = type;
    this.area = area;
    this.circumference = circumference;
    
    this.toString = function(){
        return "Obiekt to "+this.type+" o polu "+this.area+" cm3 i obwodzie "+this.circumference+" cm";
    };
}
var kwadrat = new Figure("kwadrat", 25*25, 25*4);
console.log(kwadrat.toString());

Figure.prototype.toString2 = function(){
        return "FU";
    };

var trojkat = new Figure("trójkąt", 100, 70);
console.log(trojkat.toString2());