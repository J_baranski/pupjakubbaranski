var module = {
    myProperty: 'hello',
    myConfig: {
        useCaching: true,
        language: "PL"
    },
    say: function () {
        console.log("Hejka");
    },
    report: function () {
        console.log("Caching is " + ((this.myConfig.useCaching) ? "enabled" : "disabled"));
    },
    update: function (newConfig) {
        if (typeof newConfig === "object") {
            this.myConfig = newConfig;
            console.log("Language is: " + this.myConfig.language);
        }
    }
};

/*module.say();
 module.report();
 module.update({
 language: "BR",
 catching: false
 });
 module.report();*/

/* pola prywatne */
var testModule = (function () {
    var counter = 0; 

    return {
        increment: function () {
            console.log('Counter incremented (' + (counter + 1) + ')');
            return ++counter;
        },
        reset: function () {
            counter = 0;
            console.log("Counter has been reset");
        }
    };
})();
//
//testModule.increment();
//testModule.increment();
//testModule.reset();

/* revealing module pattern */

var myRevModule = (function () { /*zwraca obiekt, ktory nastepnie jest przypisywany do myRevModule*/
    var privateCounter = 0;

    function privateFunction() {
        privateCounter++;
    }
    function publicIncrement() {
        privateFunction();
    }

    function publicFunction() {
        publicIncrement();
    }

    function publicGetCount() {
        return privateCounter;
    }
    
    /*
     * Pominięcie nawiasów powoduje przekazanie całego kodu funckji, 
     * zamiast jedynie wyniku jej działania. Funkcja privateFunction nie jest przekazywana, 
     * a wiec pozostaje prywatna dla obiektu
     * */
    
    return {
        start: publicFunction,
        increment: publicIncrement,
        count: publicGetCount
    };
})();

myRevModule.start();

