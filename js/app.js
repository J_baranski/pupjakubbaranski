/*Dziala tylko w js, bo w innych jezykach 'i' bedzie wykasowane z pamieci po petli*/
function stringLen(str) {
    for (var i in str)
        ;
    return ++i;
}
/*
 * Funkcja liczy wystepowanie danego znaku w ciagu znakow i wstawia te wartosc do 
 *tablicy asocjacyjnej z lietrami jako kluczami
 *Mozna odwolac sie do elementów tablicy asocjacyjnej poprzez iterowanie sie przez
 *ciag znakow i wywolywanie indeksy tablicy asocjacyjnej podstawiajac kolejne litery*/
function charCount(str) {
    var arr = {};
    for (var i in str) {
        (arr[str[i]]) ? arr[str[i]] += 1 : arr[str[i]] = 1;
    }
    return arr;
}

/**
 * Napisz funkcję, która zwróci dla zadanego ciągu znaków nowy ciąg składający 
 * się z pierwszych trzech oraz ostatnich trzech znaków ciągu. W przypadku, gdy 
 * ciąg ma mniej niż 3 znaki - zwróć pustą wartość. Nie używaj funkcji substring.
 * */

function scyther(str) {
    if (str.length < 3)
        return "";
    var arr = [];
    for (var i = 0; i < 3; i++) {
        arr[i] = str[i];
        arr[3 + i] = str[str.length - 3 + i];
    }
    return arr.join('');
}

/**
 * Napisz funkcję, która dla dwóch zadanych ciągów znaków, zwróci jeden, 
 * rozdzielony spacją z zamienionymi pierwszymi znakami każdego łańcucha. 
 * Przykład: Wejście: 'javascript', 'zadanie' (jako arg. f-cji) Wyjście: 
 * 'zavascript jadanie'. W tym zadaniu nie używajmy tablic.
 */

function stringMixer() {
    return arguments[1].substring(0, 1) + arguments[0].substring(1) + " " +
            arguments[0].substring(0, 1) + arguments[1].substring(1);
}

/*
 * Angielski. Napisz funkcję, która dla zadanego łańcucha znaków doda końcówkę 
 * 'ing'. Jeżeli ciąg znaków już kończy się na 'ing' - zamiast tego należy 
 * dodać 'ly'. Jeżeli łańców znaków ma długość krótszą niż 3 znaki - pozostaw 
 * ciąg bez zmian
 * W tym zadaniu nie używajmy tablic
 */

function englishy(str) {
    if (str.length < 3)
        return str;
    if (str.substring(str.length - 3) === 'ing')
        return str + 'ly';
    if (str.substring(str.length - 1) === 'e')
        return str.substring(0, str.length - 1) + 'ing';
    return str + 'ing';
}

/*
 *   
 Napisz funkcję, która znajdzie pierwsze wystąpienie słowa 'nie' oraz 'zly' w 
 zadanym ciągu znaków. Jeżeli słowo 'nie' występuje przed słowem 'zly' (pomiędzy 
 nimi mogą znajdować się inne słowa), zastąp całość słowem 'nienajgorszy'. 
 Dla przykładu: 
 Wejście: Tekst jest niezly! 
 Wyjście: Tekst jest nienajgorszy! 
 
 Wejście: Tekst jest nie taki zly! 
 Wyjście: Tekst jest nienajgorszy!
 */

function changer(str) {
    var start = str.indexOf('nie');
    var end = str.indexOf('zly', start);
    return str.substring(0, start) + 'nienajgorszy' + str.substring(end + 3);

}

/*
 * Napisz funkcję, która dla zadanego ciągu znaków oraz numerze indeksu, 
 * zwróci ten ciąg bez znaku, który znajduje się na zadanym miejscu. Wydrukuj 
 * informację, jeżeli zadany indeks jest większy niż długość ciągu. Przykład:
 
 Wejście (argumenty): 'javascript', 0 
 Wyjście: 'avascript' 
 
 Wejście: 'javascript', 5 
 Wyjście: 'javacript'
 
 Wskazówka, żeby było łatwiej: Nie uważaj tablic oraz funkcji substring(), slice() 
 */

/* W foreachu i nie jest numberem tylko stringiem */
function deleter(str, index) {
    if (index > str.length - 1)
        throw "IndexOutOfBoundsException";
    var strBuilder = '';
    for (var i in str) {
        if (i !== index)
            strBuilder += str[i];
    }
    return strBuilder;
}
/*
 * zamienia litery pierwsza i ostatnia
 */
function replacer(str) {
    temp = '';
    for (var i = 1; i < str.length - 1; i++)
        temp += str[i];
    return str[str.length - 1] + temp + str[0];
}


/*
 * Napisz funkcję, która usunie z zadanego ciągu znaków znaki o nieparzystych indeksach.
 */

function odder(str) {
    var temp = '';
    for (var i in str) {
        if (i % 2 === 0)
            temp += str[i];
    }
    return temp;
}
function odderNoModulo(str) {
    var temp = '';
    for (var i = 0; i < str.length; i += 2)
        temp += str[i];
    return temp;
}

/*
 * Napisz funkcję, która zliczy ile razy występuje każde ze słów POD 
 * NIEPARZYSTYM INDEKSEM w przekazanym zdaniu jako argument.
 * Dla ułatwienia użyj tablicy asocjacyjnej.
 */

function wordCounter(str) {
    dict = {};
    arr = str.split(' ');
    for (var i = 1; i < arr.length; i += 2) {
        (dict[arr[i]]) ? dict[arr[i]]++ : dict[arr[i]] = 1;
    }
    return dict;
}

/*
 * Napisz funkcję, która w zadanej liście zliczy ilość słów, które zaczynają i 
 * kończą się tą samą literą. Pomiń obliczenia dla słów, które mają mniej niż 
 * 3 znaki.Dla ułatwienia koniecznie użyj funkcji charAt()
 */

function sameLetter(arr) {
    var ct = 0;
    for (var i in arr) {
        if (arr[i].length < 3)
            continue;
        if (arr[i].charAt(0) === arr[i].charAt(arr[i].length - 1))
            ct++;
    }
    return ct;
}

/*
 * Napisz funkcję, która usunie duplikaty liczb znajdujących się na liście. 
 * Przykład: 
 * Wejście: [1, 2, 3, 2, 3, 2, 4] 
 * Wyjście: [1, 2, 3, 4]
 */

<<<<<<< HEAD
function duplicates(arr) {
    var newTab = [];
    for (var i = 0; i < arr.length; i++) {
        if (arr.indexOf(arr[i], i + 1) == -1)
            newTab.push(arr[i]);
=======
function duplicates(arr){
    var newTab = [];
    for(var i = 0; i< arr.length; i++){
        if(arr.indexOf(arr[i], i+1) == -1) newTab.push(arr[i]);
>>>>>>> f811e7aa726f04d9de5acec0887c9154c314c482
    }
    return newTab;
}

/*
 * 
 */

<<<<<<< HEAD
function squares() {
    d = {};
    for (var i = 1; i < 11; i++) {
        d[i] = i * i;
    }
    return d;
}
/*
 * Liczy sumę zagnieżdżonych elementów tablicy
 */
function nestedSum(arr) {
    var sum = 0;
    if (typeof (arr) === "number")
        return arr;
    if (typeof (arr) === "object") {
        for (var i in arr) {
            if (typeof (arr[i]) === "number")
                sum += arr[i];
            else if (typeof (arr[i]) === 'object')
                sum += nestedSum(arr[i]);
        }
        return sum;
    }

}

//console.log(nestedSum([[1, 2], [[3, 4], [5, 6]], [7], 8]));
//console.log(nestedSum([1, 'siema', [1,3]]));

/*
 * Napisz funkcję, która przyjmuje dwa argumenty dwa słowniki, które mają 
 * przypisane wartości w stylu { 'string' : number, ... } i zwraca jeden, który 
 * jest połączeniem tych dwóch argumentów. Jeżeli w obu słownikach występuje ten 
 * sam klucz, to wartość numeryczna powinna być sumą wartościa danego klucza dla 
 * dwóch słowników.
 
 Przykład:
 
 Weście: var x = { 'a' : 1, 'b' : 2 }
 y = { 'a' : 3, 'c' : 4}
 
 Wyjście: { 'a' : 4, 'b' : 2, 'c' : 4 } /* kolejność argumentów jest dowolna */

function combiner(argOne, argTwo) {
    for (var key in argOne) {
        if (argOne.hasOwnProperty(key)) {
            if (argTwo[key])
                argTwo[key] += argOne[key];
            else
                argTwo[key] = argOne[key];
        }
    }
    return argTwo;
}
//var x = { 'a' : 1, 'b' : 2 },
//y = { 'a' : 3, 'c' : 4};
//
//console.log(combiner(x,y));

/**
 * Funkcja przyjmuje dwa słowniki (jako argumenty). Korzystając z metody charCodeAt() 
 * zbadajmy, czy wartość pod danym kluczem jest parzysta czy nie. Jeżeli pierwsza 
 * litera w kluczu ma wartość parzystą i wartość klucza jest także liczbą parzystą, 
 * dodajmy ten klucz do tablicy ( [] ), jeżeli obie wartości są nieparzyste także 
 * dodajemy klucz do tablicy. W przypadku gdy wartość klucza jest nieparzysta, a 
 * pierwsza litera klucza ma wartość parzystą taki klucz pomijamy - podobnie w 
 * sytuacji odwrotnej. Podsumowując, w tablicy powinny się znaleźć tylko klucze, 
 * które spełniają dane zależności:
 * > parzystość pierwszej litery klucza i wartości klucza musi być taka sama
 
 Podpowiedź: console.log('a'.charCodeAt(0));    
 
 Wejście: var x = { 'pierwszy' : 'a', 'drugi' : 'b', 'trzeci' : 'c' },
 y = { 'czwarty'  : 'd', 'piaty' : 'e', 'szosty' : 'f' };
 */

function what(argOne, argTwo) {
    var out = [];
    for (var key in argTwo) {
        argOne[key] = argTwo[key]
    }
    for (var key in argOne) {
        if (key.charCodeAt(0) % 2 === 0) {
            if (argOne[key].charCodeAt(0) % 2 === 0)
                out.push(key);
        }
        if (key.charCodeAt(0) % 2 !== 0) {
            if (argOne[key].charCodeAt(0) % 2 !== 0)
                out.push(key);
        }
    }
    return out;
}
var x = {'pierwszy': 'a', 'drugi': 'b', 'trzeci': 'c'};
//y = {'czwarty': 'd', 'piaty': 'e', 'szosty': 'f', 'siodmy': 'g'};
//console.log(what(x, y));

/*
 * Mamy zdefiniowany słownik, który jest przekazany dla funkcji jako argument. 
 * Zwróćmy słownik, który będzie miał przypisane wartości jako klucze, a klucze 
 * jako wartości.
 */

function keySwapper(arg) {
    var out = {};
    for (var key in arg) {
        out[arg[key]] = key;
    }
    return out;
}

function median(arg) {
    var out = [];
    for (var key in arg) {
        out.push(arg[key]);
    }
    out = selectionSort(out);
    if (out.length % 2 === 0) {
        return (out[((out.length / 2) - 1)] + out[(out.length / 2)]) / 2;
    } else {
        return out[Math.floor(out.length / 2)];
    }
}

function selectionSort(arr) {
    var minI;
    var temp;
    for (var i = 0; i < arr.length; i++) {
        minI = i;
        for (var j = i + 1; j < arr.length; j++) {
            if (arr[j] < arr[i])
                minI = j;
        }
        if (minI !== i) {
            temp = arr[i];
            arr[i] = arr[minI];
            arr[minI] = temp;
        }
    }
    return arr;
}

//console.log(median( { 'a' : 15, 'b' : 7, 'c' : 2, 'd' : 9, 'e' : 11, 'f' : 3, 'g' : -20, 'i' : 13 }));
//console.log(median( { 'a' : 15, 'b' : 7, 'c' : 2, 'd' : 9, 'e' : 11, 'f' : 3, 'g' : -20 } ));

/*
 * 
 */

function sum2DArray(arr) {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            sum += arr[i][j];
        }
    }
    return sum;
}

var x = [[1, 2, 9], [3, 4, 9], [5, 6, 9], [7, 8, 9]];
//console.log(sum2DArray(x));

/*
 * tablice 2-wymiarowe
 */

/*
 * program sumujacy elementy podzielne przez 7
 */
function sumDiv7(arg) {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            if (arr[i][j] % 7 == 0)
                sum += arr[i][j];
        }
    }
    return sum;
}

/*
 * iloczyn
 */

function multiply2DArray(arr) {
    var prod;
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            if (!prod)
                prod = arr[i][j];
            else
                prod *= arr[i][j];
        }
    }
    return prod;
}

/*
 * iloczyn parzystych elementów
 */
function multiplyEven2DArray(arr) {
    var prod;
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            if (arr[i][j] % 2 === 0) {
                if (!prod)
                    prod = arr[i][j];
                else
                    prod *= arr[i][j];
            }

        }
    }
    return prod;
}

/*
 * iloczyn nieparzystych
 */

function multiplyOdd2DArray(arr) {
    var prod;
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            if (arr[i][j] % 2 !== 0) {
                if (!prod)
                    prod = arr[i][j];
                else
                    prod *= arr[i][j];
            }

        }
    }
    return prod;
}

/*
 * iloczyn elementow podzielnych przez 7
 */

function multiplyDiv72DArray(arr) {
    var prod;
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].length; j++) {
            if (arr[i][j] % 7 === 0) {
                if (!prod)
                    prod = arr[i][j];
                else
                    prod *= arr[i][j];
            }

        }
    }
    return prod;
}


/*Nudy...*/

/*
 * Napisz obiekt, a w nim funkcje które dla wartości wczytanej z klawiatury zliczą ilość występujących
 liter oraz cyfr we wczytanym ciągu. Obiekt powinien posiadać funkcję sprawdzającą czy dany znak
 jest cyfrą, literą oraz funkcję dokonującą zliczeń. Obiekt powinien posiadać argument przy tworzeniu
 instancji, który będzie stringiem
 */

//var str = prompt("Wprowadź ciąg znaków, ktory ma zostac poddany analizie", 'str');
//var word = new WordAnalyse(str);
//console.log(word.str + " W wyrazie jest " + word.countLetters() + " liter i "+word.countNumers()+" cyfr");

function WordAnalyse(arg) {
    this.str = arg;


    this.isLetter = function (ch) {
        ch = ch.toUpperCase();
        var code = ch.charCodeAt(0);
        if (code > 64 && code < 91)
            return true;
        else
            return false;
    };

    this.isNumber = function (ch) {
        ch = ch.toUpperCase();
        var code = ch.charCodeAt(0);
        if (code > 48 && code < 58)
            return true;
        else
            return false;
    };

    this.countLetters = function () {
        var ct = 0;
        for (var i in this.str) {
            if (this.isLetter(str[i]))
                ++ct;
        }
        return ct;
    };

    this.countNumers = function () {
        var ct = 0;
        for (var i in this.str) {
            if (this.isNumber(str[i]))
                ++ct;
        }
        return ct;
    };
}

/*
 * Napisz obiekt, który będzie posiadał min. dwie metody, wyswietl() oraz wczytaj(). Metoda wczytaj()
powinna zapytać użytkownika o ciąg znaków, metoda wyświetl() powinna przyjąć co najmniej jeden
argument, który w zależności od wyboru ('duze', 'male') wyświetli pobrany ciąg znaków dużymi lub
małymi literami
 */

function Caser(str){
    this.str = str;
    
    this.wczytaj = function(str){
        if(typeof(str) !== 'string' ) throw "W argumencie nalezy podac string!";
        else this.str = str;
    };
    
    this.wyswietl = function(){
        if(arguments[0] === 'duze'){
            console.log(this.str.toUpperCase());
        }
        else if(arguments[0] === 'male'){
            console.log(this.str.toLowerCase());
        }
        else if(arguments.length === 0){
            console.log(this.str);
        }
        else throw "Error: Niewlasciwy argument. Funkcja przyjmuje stringi 'duze' i 'male'";
    };
}


=======
function squares (){
    d = {};
    for (var i = 1; i< 11; i++){
        d[i] = i*i;
    }
    return d;
}
>>>>>>> f811e7aa726f04d9de5acec0887c9154c314c482
