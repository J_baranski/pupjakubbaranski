/*Description:

We've got a message from the Librarian. As usual there're many o and k in it and, as all codewarriors don't know "Ook" language we need that you translate this message.

tip : it seems traditional "Hello World!" would look like : Ok, Ook, Ooo? Okk, Ook, Ok? Okk, Okk, Oo? Okk, Okk, Oo? Okk, Okkkk? Ok, Ooooo? Ok, Ok, Okkk? Okk, Okkkk? Okkk, Ook, O? Okk, Okk, Oo? Okk, Ook, Oo? Ook, Ooook!

Your task is to implement a function okkOokOo(okkOookk), that would take the okkOookk message as input and return a decoded human-readable string.*/

function okkOokOo(okkOookk){
  var array = okkOookk.split('?  ');
  var codePoint;
  var out = '';
  for(var i in array){
      array[i] = array[i].split(', ').join('');
      if(array[i][array[i].length-1] =='!') array[i] = array[i].substring(0, array[i].length-1);
      array[i] = array[i].split('').reverse().join('').toLowerCase();    
  }
  for(var i in array){
      codePoint = 0;
      for(var j in array[i]){
          if(array[i][j] === 'k'){
              codePoint += Math.pow(2, +j);      
          }
      }
      out += String.fromCharCode(codePoint);
  }
  return out;
}
