/*Description:

Given two strings s1 and s2, we want to visualize how different the two strings are. We will only take into account the lowercase letters (a to z). First let us count the frequency of each lowercase letters in s1 and s2.

s1 = "A aaaa bb c"

s2 = "& aaa bbb c d"

s1 has 4 'a', 2 'b', 1 'c'

s2 has 3 'a', 3 'b', 1 'c', 1 'd'

So the maximum for 'a' in s1 and s2 is 4 from s1; the maximum for 'b' is 3 from s2. In the following we will not consider letters when the maximum of their occurrences is less than or equal to 1.

We can resume the differences between s1 and s2 in the following string: "1:aaaa/2:bbb" where 1 in 1:aaaa stands for string s1 and aaaa because the maximum for a is 4. In the same manner 2:bbb stands for string s2 and bbb because the maximum for b is 3.

The task is to produce a string in which each lowercase letters of s1 or s2 appears as many times as its maximum if this maximum is strictly greater than 1; these letters will be prefixed by the number of the string where they appear with their maximum value and :. If the maximum is in s1 as well as in s2 the prefix is =:.

In the result, substrings will be in decreasing order of their length and when they have the same length sorted alphabetically (more precisely sorted by codepoint); the different groups will be separated by '/'.*/

function mix(s1, s2) {
    var s1Count = {};
    var s2Count = {};
    var out = [];
    for (var i in s1) {
        if (s1[i] === s1[i].toLowerCase() && s1[i].toLowerCase().charCodeAt(0) > 96 && s1[i].toLowerCase().charCodeAt(0) < 123 )
            s1Count[s1[i]] ? s1Count[s1[i]]++ : s1Count[s1[i]] = 1;
    }
    for (var i in s2) {
        if (s2[i] === s2[i].toLowerCase() && s2[i].toLowerCase().charCodeAt(0) > 96 && s2[i].toLowerCase().charCodeAt(0) < 123 )
            s2Count[s2[i]] ? s2Count[s2[i]]++ : s2Count[s2[i]] = 1;
    }
    for (var key in s1Count) {
        if (s1Count[key] > s2Count[key])
            out.push('1:' + key.repeat(s1Count[key]));
        else if (s1Count[key] < s2Count[key])
            out.push('2:' + key.repeat(s2Count[key]));
        else if(!s2Count[key])
            out.push('1:' + key.repeat(s1Count[key]));
        else out.push('=:' + key.repeat(s1Count[key]));
        delete s1Count[key];
        delete s2Count[key];
    }
    for (var key in s2Count) {
        if (s2Count[key] > 1)
            out.push('2:' + key.repeat(s2Count[key]));
    }
    for (var i = 0; i < out.length; i++) {
        if (out[i].length < 4) {
            out.splice(i, 1);
            i--;
        }
    }
    out.sort();
    
    var count;
    var temp;
    do {
        count = 0;
        for (var i = 1; i < out.length; i++) {
            if (out[i - 1].length < out[i].length) {
                temp = out[i];
                out[i] = out[i - 1];
                out[i - 1] = temp;
                count++;
            }
        }
    } while (count !== 0);
    return out.join('/');
}
