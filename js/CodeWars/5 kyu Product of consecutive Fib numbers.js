/*
Description:

The Fibonacci numbers are the numbers in the following integer sequence (Fn):

0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, ...
such as

F(n) = F(n-1) + F(n-2) with F(0) = 0 and F(1) = 1.
Given a number, say prod (for product), we search two Fibonacci numbers F(n) and F(n+1) verifying

F(n) * F(n+1) = prod.
Your function productFib takes an integer (prod) and returns an array:

[F(n), F(n+1), true] or {F(n), F(n+1), 1} or (F(n), F(n+1), True)
depending on the language if F(n) * F(n+1) = prod.

If you don't find two consecutive F(m) verifying F(m) * F(m+1) = prodyou will return

[F(m), F(m+1), false] or {F(n), F(n+1), 0} or (F(n), F(n+1), False)
F(m) being the smallest one such as F(m) * F(m+1) > prod.
*/

function productFib(prod){
var currentN=0;
var currentProd=0;
var boolProd = false
var fibOne, fibTwo;
 while(currentProd < prod){
   ++currentN;
   fibOne = fib(currentN);
   fibTwo = fib(currentN+1);
   currentProd = fibOne * fibTwo ;
   if(currentProd == prod){
     boolProd = true;
     break;
   } 
 }
 return[fibOne, fibTwo, boolProd]
}

function fib(n){
	if(n==1) return 1;
	else if (n===0) return 0;
	else return fib(n-1) + fib(n-2);
}
