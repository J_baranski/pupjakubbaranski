/*Description:

In order to stop too much communication from happening, your overlords declare that you are no longer allowed to use certain functionality in your code!

Disallowed functionality:

Strings
Numbers
Regular Expressions
Functions named "Hello", "World", "HelloWorld" or anything similar.
Object keys named "Hello", "World", "HelloWorld" or anything similar.
Without using the above, output the string "Hello World!" to prove that there is always a way.*/

var helloWorld = function () {
  var d;
  var a = {H: d, e: d, l:d,};
  var b = {l:d, o:d};
  var c = { W:d, o:d, r:d, l:d,  d:d,};
  var space = [,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,]
  var exclamation = [,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,]
  var out;
  var init = false;
  for (var key in a){
      if(init == true) out += key;
      else {
          out = key;
          init = true;
      }
  }
  for (var key in b){
     out += key;
  }
  out += String.fromCharCode(space.length)
  var out;
  for (var key in c){
      out += key;
  }
  out += String.fromCharCode(exclamation.length);
  return out;
}
