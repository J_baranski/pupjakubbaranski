1.1 SELECT * FROM pracownik;
1.2 SELECT imie FROM pracownik;
1.3 SELECT imie, nazwisko, dzial FROM pracownik;

2.1 SELECT imie, nazwisko, pensja FROM pracownik ORDER BY pensja DESC;
2.2 SELECT nazwisko, imie FROM pracownik ORDER BY nazwisko , imie ;
2.3 SELECT nazwisko, dzial, stanowisko FROM pracownik ORDER BY dzial ASC, stanowisko DESC;

3.1 SELECT DISTINCT dzial FROM pracownik;
3.2 SELECT DISTINCT dzial, stanowisko FROM pracownik;
3.3 SELECT DISTINCT dzial, stanowisko FROM pracownik ORDER BY dzial DESC, stanowisko DESC;

4.1 SELECT imie, nazwisko FROM pracownik WHERE imie='JAN' ;
4.2 SELECT imie, nazwisko FROM pracownik WHERE stanowisko='SPRZEDAWCA';
4.3 SELECT imie, nazwisko, pensja FROM pracownik WHERE pensja>1500 ORDER BY pensja DESC;

5.1 SELECT imie, nazwisko, dzial, stanowisko FROM pracownik WHERE dzial='OBSLUGA KLIENTA' AND stanowisko='SPRZEDAWCA';
5.2 SELECT imie, nazwisko, dzial, stanowisko FROM pracownik WHERE dzial='TECHNICZNY' AND stanowisko='KIEROWNIK' OR stanowisko='MECHANIK';
5.3 SELECT * FROM samochod WHERE NOT marka='FIAT' AND NOT marka='FORD';

6.1 SELECT * FROM samochod WHERE marka IN ('MERCEDES', 'SEAT', 'OPEL');
6.2 SELECT imie, nazwisko, data_zatr FROM pracownik WHERE imie IN ('ANNA', 'MARZENA', 'ALICJA');
6.3 SELECT imie, nazwisko, miasto FROM klient WHERE NOT miasto='WARSZAWA' AND NOT miasto='WROCLAW';

7.1 SELECT imie, nazwisko FROM klient WHERE nazwisko LIKE 'K%';
7.2 SELECT imie, nazwisko FROM klient WHERE nazwisko LIKE 'D%SKI';
7.3 SELECT imie, nazwisko FROM klient WHERE nazwisko LIKE '_O%' OR nazwisko LIKE '_A%';

8.1 SELECT * FROM samochod WHERE poj_silnika BETWEEN 1100 AND 1600;
8.2 SELECT * FROM pracownik WHERE data_zatr BETWEEN '1997-01-01' AND '1997-12-31';
8.3 SELECT * FROM samochod WHERE przebieg BETWEEN 10000 AND 20000 OR przebieg BETWEEN 30000 AND 40000;

9.1 SELECT * FROM pracownik WHERE dodatek IS NULL;
9.2 SELECT * FROM klient WHERE nr_karty_kredyt IS NOT NULL;
9.3 SELECT imie, nazwisko, COALESCE(dodatek, 0) FROM pracownik;

10.1 SELECT imie, nazwisko, pensja, pensja + COALESCE(dodatek, 0) AS do_zaplaty FROM pracownik;
10.2 SELECT imie, nazwisko, pensja*1.5 AS nowa_pensja FROM pracownik;
10.3 SELECT imie, nazwisko, (pensja + COALESCE(dodatek, 0))*0.01 AS procent FROM pracownik ORDER BY procent;

11.1 SELECT w.id_samochod, s.marka, s.typ, w.data_wyp, w.data_odd FROM wypozyczenie w LEFT JOIN samochod s ON w.id_samochod = s.id WHERE data_odd IS NULL; 
11.2 SELECT k.imie, k.nazwisko, w.id_samochod, w.data_wyp FROM wypozyczenie w LEFT JOIN klient k ON w.id_klient = k.id WHERE data_odd IS NULL ORDER BY k.nazwisko, k.imie, w.data_wyp;
11.3 SELECT k.imie, k.nazwisko,  w.data_wyp AS data_kaucji , w.kaucja FROM wypozyczenie w LEFT JOIN klient k ON w.id_klient = k.id WHERE kaucja IS NOT NULL;

12.1 SELECT k.imie, k.nazwisko, s.marka, s.typ  FROM wypozyczenie w LEFT JOIN klient k ON w.id_klient = k.id LEFT JOIN samochod s ON w.id_samochod = s.id WHERE data_wyp IS NOT NULL ORDER BY k.nazwisko, k.imie, s.typ;
12.2 SELECT m.ulica, m.numer, s.marka, s.typ FROM wypozyczenie w LEFT JOIN miejsce m ON w.id_miejsca_wyp = m.id LEFT JOIN samochod s ON w.id_samochod = s.id WHERE data_wyp IS NOT NULL ORDER BY m.ulica, m.numer, s.marka, s.typ;
12.3 SELECT s.id, s.marka, s.typ, k.imie, k.nazwisko FROM wypozyczenie w LEFT JOIN klient k ON w.id_klient = k.id LEFT JOIN samochod s ON w.id_samochod = s.id ORDER BY s.id, k.nazwisko, k.id;

13.1 SELECT MAX(pensja) FROM pracownik;
13.2 SELECT AVG(pensja) FROM pracownik;
13.3 SELECT MIN(data_prod) FROM samochod;
(
14.1 SELECT imie, nazwisko, pensja FROM pracownik WHERE pensja = (SELECT MAX(pensja) FROM pracownik);
14.2 SELECT imie, nazwisko, pensja FROM pracownik WHERE pensja > (SELECT AVG(pensja) FROM pracownik);
14.3 SELECT marka, typ, data_prod FROM samochod WHERE data_prod = (SELECT MIN(data_prod) FROM samochod);

/*15.1 SELECT marka, typ, data_prod FROM samochod s LEFT JOIN wypozyczenie w ON w.id_samochod = s.id WHERE w.data_wyp IS NULL;
15.2 SELECT imie, nazwisko FROM klient k LEFT JOIN wypozyczenie w ON w.id_klient = k.id WHERE w.data_wyp IS NULL ORDER BY nazwisko, imie;
15.3 SELECT id, imie, nazwisko FROM pracownik p LEFT JOIN wypozyczenie w ON w.id_pracow_wyp = p.id WHERE w.data_wyp IS NULL;
//bez joinowania*/

15.1 SELECT marka, typ, data_prod FROM samochod WHERE id NOT IN (SELECT id_samochod FROM wypozyczenie WHERE data_wyp IS NULL);
15.2 SELECT imie, nazwisko FROM klient WHERE id IN (SELECT id_klient FROM wypozyczenie WHERE data_wyp IS NULL);
15.3 SELECT imie, nazwisko FROM pracownik WHERE id NOT IN (SELECT id_pracow_wyp FROM wypozyczenie WHERE data_wyp IS NOT NULL);

16.1 UPDATE pracownik set dodatek=50 WHERE dodatek IS NULL;
16.2 UPDATE klient SET imie=JERZY, nazwisko=NOWAK WHERE id=10;
16.3 UPDATE pracownik SET pensja=pensja*1.1 WHERE pensja < (SELECT AVG(pensja) FROM pracownik);

17.1 DELETE FROM pracownik WHERE id=17;
17.2 DELETE FROM wypozyczenie WHERE id_klient=17;
17.3 DELETE FROM klient WHERE id NOT IN (SELECT id_klient FROM wypozyczenie WHERE data_wyp IS NOT NULL);

18.1 INSERT INTO klient VALUES (21, 'ADAM', 'CICHY', NULL, NULL, 'KORZENNA', 12, 'WARSZAWA', '00-950', NULL, '123-454-321');
18.2 INSERT INTO samochod VALUES (19, 'SKODA', 'OCTAVIA', '2012-09-01', 'SREBRNY', 1896, 5000);
18.3 INSERT INTO wypozyczenie VALUES (26, 21, 19, 1, NULL, 2, NULL, '2012-10-28', NULL, 4000, 500);

19.1 SELECT imie, nazwisko, data_zatr FROM pracownik WHERE EXTRACT(MONTH FROM data_zatr) = 5;
19.2 SELECT imie, nazwisko, DATE_PART ('day', now() - data_zatr) AS dni FROM pracownik ORDER BY dni DESC;
19.3 SELECT k.imie, k.nazwisko, s.marka, s.typ, DATE_PART('day', now() - data_wyp) AS dni 
FROM wypozyczenie w 
LEFT JOIN samochod s ON w.id_samochod = s.id 
LEFT JOIN klient k ON w.id_klient = k.id 
WHERE data_odd IS NULL ORDER BY dni DESC;

20.1 SELECT imie, nazwisko, LEFT(imie, 1) || '.' || LEFT(nazwisko, 1) ||'.' AS inicjal FROM klient ORDER BY inicjal, nazwisko, imie; 
20.2 SELECT LEFT(imie, 1) || lower(RIGHT(imie, -1)) AS Imie, LEFT(nazwisko, 1) || lower(RIGHT(nazwisko, -1)) AS Nazwisko FROM klient;
20.3 SELECT imie, nazwisko, LEFT(nr_karty_kredyt, -6) || '******' AS nr_karty_kredyt FROM klient;


