$(document).ready(function () {

    var clientList = {111: new Client('admin', 'adminowski', 111)};
    var signedClient;

    $('.registerBox').hide();
    $('.panel').hide();

    /*listener dla przycisku rejestracji*/
    $('.registerBtn').on('click', function () {
        $('.registerBox').fadeIn();
    });
    $('.registerClose').on('click', function () {
        $('.registerBox').fadeOut();
    });

    /*ClearLog*/
    $('.logClear').on('click', clearLog);

    function clearLog() {
        $('.log').text(' ').css('color', 'black');
    }

    /*Rejestracja*/
    $('.registerConfirm').on('click', function () {
        clearLog();
        if (clientList[$('.regPeselInput').val()]) {
            $('.registerLog').text('Taki PESEL juz znajduje sie w bazie');
            return;
        }
        if ($('.regPeselInput').val().length !== 11) {
            $('.registerLog').text("Nieprawidlowa liczba znakow").css('color', 'red');
            return;
        }

        $('.registerLog').text(' ');
        var client = new Client($('.nameInput').val(), $('.surInput').val(), $('.regPeselInput').val());
        clientList[$('.regPeselInput').val()] = client;
        $('.registerBox').fadeOut();
    });

    /*Login*/
    $('.loginBtn').on('click', function () {
        clearLog();

        if (!clientList[$('.logPeselInput').val()])
            $('.log').text("Wpisany PESEL nie znajduje sie obecnie w bazie danych").css('color', 'red');
        else {
            signedClient = clientList[$('.logPeselInput').val()];
            $('.panel').show();
            $('.loginBox').hide();
            $('.value').text(signedClient.account);
            $('.hello').text('Witaj ' + signedClient.name);
        }
    });

    /*Logout*/
    $('.logout').on('click', function () {
        signedClient = null;
        $('.loginBox').show();
        $('.panel').hide();
        $('.logPeselInput').val('');
        clearLog();
    });

    /*Deposit*/
    $('.deposit').on('click', function () {
        clearLog();
        var value = 0;
        value = prompt('Ile chcialbys wpłacić?');
        signedClient.deposit(value);
        $('.value').text(signedClient.account);
    });

    /*Withdraw*/
    $('.withdraw').on('click', function () {
        clearLog();
        $('.log').text(' ');
        var value = 0;
        value = prompt('Ile chcialbys wypłacić?');
        if (value > signedClient.account) {
            $('.log').text('Brak wystarczajacych srodkow na koncie');
            return;
        } else {
            signedClient.withdraw(value);
            $('.value').text(signedClient.account);
        }
    });

    /*Show History*/
    $('.history').on('click', function () {
        clearLog();
        if (signedClient.history.empty()) {
            $('.log').text('Brak operacji do wyswietlenia');
            return;
        }
        clearLog();
        var histArray = []

        var size = signedClient.history.size();
        for (var i = 0; i < size; i++) {

            var temp = signedClient.history.pop();
            if (temp < 0)
                $('.log').html($('.log').html() +  '<span style="color: red;">Wypłata: ' + Math.abs(temp) + ' PLN'+'</span><br/>');
            else if (temp > 0)
                $('.log').html($('.log').html() + '<span style="color: green;">Wpłata: ' + Math.abs(temp) + ' PLN'+ '</span><br/>');
            histArray.push(temp);
        }
        for (var i = histArray.length - 1; i >= 0; i--) {
            signedClient.history.push(histArray[i]);
        }
    });

    function Client(name, surname, pesel) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.account = 0;
        this.history = new Stack();

        this.deposit = function (a) {
            var ammount = Number(a);
            if (isNaN(ammount)) {
                console.log("Wrong input value");
                return;
            }
            this.account += Math.abs(ammount);
            this.history.push(Math.abs(ammount));

        };

        this.withdraw = function (a) {
            var ammount = Number(a);
            if (isNaN(ammount)) {
                console.log("Wrong input value");
                return;
            }
            if (ammount > this.account) {
                return;
            } else {
                console.log('wyplacam');
                this.account -= Math.abs(ammount);
                this.history.push(-1 * Math.abs(ammount));
            }
        };
    }
    ;

    function Stack() {

        var array = [];
        function empty() {
            if (array.length === 0)
                return true;
            else
                return false;
        }
        ;
        function pop() {
            if (!empty())
                return array.pop();
            else
                return null;
        }
        ;
        function push(item) {
            array.push(item);
        }
        ;
        function size() {
            return array.length;
        }
        ;
        function top() {
            if (!empty())
                return array.length - 1;
            else
                return null;
        }
        ;
        return{
            empty: empty,
            pop: pop,
            push: push,
            size: size,
            top: top
        };
    }
});

