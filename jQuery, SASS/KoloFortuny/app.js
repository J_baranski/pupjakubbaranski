$(document).ready(function () {
    var tab = ['SLOWO', 'INNE', 'KARAMBA', 'TRAGEDIA', 'DRAMAT', 'owoc', 'magia', 'ludzie', 'fortuna', 'obecnosc', 'heheszki'];
    var pickedWord = tab[Math.floor(Math.random() * tab.length) | 0].toUpperCase();
    var usedLetters = [];
    var triesLeft = pickedWord.length * 2;
    var triesInput = 3;
    var codedWord = '_'.repeat(pickedWord.length);
    /*tworzenie dokumentu*/
    $('<div/>', {class: 'rightSide'}).prependTo('.fortune')
    $('<div/>', {class: 'leftSide'}).prependTo('.fortune')
    $('<div/>', {class: 'guessBox'}).prependTo('.rightSide');
    $('<input/>', {'id': 'input', 'type': 'text', 'value': ''}).prependTo('.guessBox');
    $('<button>Zgaduj</button>', {'id': 'guess'}).appendTo('.guessBox');
    $('<div/>', {class: 'triesInput'}).html('Pozostała ilość prób: <span>' + triesInput + '</span>').appendTo('.guessBox');
    $('<div/>', {class: 'usedLetterBox'}).html('Wykorzystane litery: <span class="usedLetters"></span>').appendTo('.rightSide')
    $('<div/>', {class: 'buttons'}).appendTo('.rightSide');
    $('<div/>', {class: 'word'}).appendTo('.leftSide');
    $('<h1/>', {class: 'title'}).html('KOŁO FORTUNY').prependTo('.leftSide');
    $('<div/>', {class: 'winBox'}).hide().appendTo('.fortune');
    $('<span/>', {class: 'win'}).html('BRAWO! WYGRAŁEŚ! <br/>').appendTo('.winBox')
    $('<button/>', {class: 'reload'}).html("Jeszcze raz!").appendTo('.winBox');
    $('<div/>', {class: 'triesBox'}).html("Pozostało <br/><span class='tries'></span><br/>prób").appendTo('.rightSide');
    $('.tries').text(triesLeft);
    $('.word').append('<span>' + codedWord + '</span>');
    /*tworzenie przyciskow*/
    for (var i = 0; i < 26; i++) {
        $('<button/>', {id: String.fromCharCode(65 + i)}).html(String.fromCharCode(65 + i)).appendTo('.buttons');
    }
    /*eventListener dla przycisków*/
    $('.buttons button').on('click', function (e) {
        $(this).attr('disabled', 'disabled');
        var id = '#' + $(this).text();
        process(id);
    });
    /*event listener dla klawiatury*/
    $(window).on('keyup', function (e) {
        if (e.target.tagName == 'INPUT')
            return;
        var charCode = e.which;
        var id = '#' + String.fromCharCode(charCode).toUpperCase();
        $(id).attr('disabled', 'disabled');
        process(id);
    });
    /*Funkcja wywolywana przez listenery*/
    function process(id) {
        console.log($(id).text());
        /*jezeli litera byla uzyta, funkcja sie konczy*/
        if (usedLetters.indexOf($(id).text()) !== -1) {
            return;
        }
        triesLeft--;
        usedLetters.push($(id).text());
        $('.usedLetterBox span').html(($('.usedLetterBox span').html() + $(id).text()+' '));
        $('.tries').text(triesLeft);
        if(triesLeft < 4){
            $('.tries').css('color', 'red');
        }
        var indices = [];
        for (var i = 0; i < pickedWord.length; i++) {
            if (pickedWord[i] === $(id).text()) {
                indices.push(i);
            }
        }
        if (indices.length !== 0) {
            $(id).addClass('goodBtn');
        } else {
            $(id).addClass('wrongBtn')
        }
        /*zamiana liter*/
        var codedTab = codedWord.split('');
        for (var i = 0; i < indices.length; i++) {
            codedTab[indices[i]] = pickedWord[indices[i]];
        }
        codedWord = codedTab.join('');
        $('.word').find('span').text(codedWord);
        /*warunek wygranej. Wylaczyc listenery i wyswietlic komunikat.*/
        if (codedWord.indexOf("_") === -1) {
            win();
            return;
        }
        /*warunek przegranej. Wylaczyc listenery i wyswietlic komunikat.*/
        if (triesLeft === 0) {
            lose()
        }
    }
    /*listener dla inputu*/
    $('.guessBox button').on('click', function () {
        triesInput--;
        $('.triesInput').text('Pozostała ilość prób:  ' + triesInput);
        if ($('#input').val().toUpperCase() === pickedWord) {
            $('.word span').text(pickedWord);
            win();
        } else if (triesInput === 0) {
            lose();
        }
    });
    /*nowa gra*/
    $('.reload').on('click', function () {
        location.reload(true);
    });
    /*warunki wygranej i przegranej*/
    function win() {
        $(window).off('keyup');
        $('.buttons button').off('click');
        $('.winBox').fadeIn();
    }
    function lose() {
        $('.win').html("SŁABO <br/>");
        $('.winBox').fadeIn();
        $(window).off('keyup');
        $('.buttons button').off('click');
    }
});

